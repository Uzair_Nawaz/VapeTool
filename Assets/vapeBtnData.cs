using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class vapeBtnData : MonoBehaviour
{
    public Image VapeBtn;
    public void initVape(Sprite spr)
    {
        this.GetComponent<Button>().onClick.AddListener(() => {
            GameManagerVape.instance.StartGamePlay(VapeBtn.sprite);
        });
        VapeBtn.sprite = spr;
    }
}
