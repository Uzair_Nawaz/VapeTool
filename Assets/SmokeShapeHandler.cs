using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeShapeHandler : MonoBehaviour
{

    private void Start()
    {
        UserPressSmokeShapeBtn(6, true);
    }
    //public bool checkForDefaultSmoke()
    //{
    //   float val = PlayerPrefs.GetFloat(AnimationType.Default.ToString(), 1);
    //    if (val > 0)
    //    {

    //    }
    //}
    public static void UserPressSmokeShapeBtn(int mode, bool isFirstTime = false)
    {
        AnimationType smokeType = (AnimationType)mode;
        GameManagerVape.instance.SaveFluid();
        float val = 0;
        if (smokeType == AnimationType.Default)
        {
            val = PlayerPrefs.GetFloat(smokeType.ToString(), 1);
        }
        else
        {
            val = PlayerPrefs.GetFloat(smokeType.ToString(), 0);
        }
        if (isFirstTime)
        {
            val = 1;
        }
        if (val <= 0.1f)
        {
            PopupManager.instance.InstantiatePopup(PopupType.SmokeRefillPopup).GetComponent<VapeRefillHandler>().init(mode);
        }
        else
        {
            GameManagerVape.instance.CurrentSelectedSmoke = smokeType;
            LiquidFluidHandler.instance.FillFluid(val);
            SmokeController.OnSmokeShapeUpdated?.Invoke(mode);
        }
    }
}
