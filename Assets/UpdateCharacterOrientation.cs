using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateCharacterOrientation : MonoBehaviour
{
    public bool isCharacterSelectionScreen;
    bool ignoreFirstTime = true;
    public static  bool isCharacterSelectionScreenOpen;
    private void OnEnable()
    {
        if (isCharacterSelectionScreen)
        {
          CharacterController.CrntPos = CharacterPos.CenterPos;
            CharacterController.OnCharacterPosSet?.Invoke(CharacterPos.CenterPos);
            isCharacterSelectionScreenOpen = true;
        }
        else
        {
            if (ignoreFirstTime)
            {
                ignoreFirstTime = false;
                return;
            }
            CharacterController.CrntPos = CharacterPos.SidePos;
            CharacterController.OnCharacterPosSet?.Invoke(CharacterPos.SidePos);
        }
    }
    
}
