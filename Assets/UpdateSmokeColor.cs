using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UpdateSmokeColor : MonoBehaviour
{
    // Start is called before the first frame update
    Image crntImg;
    ParticleSystem system;
    
    void Start()
    {
        SmokeController.OnSmokeColorUpdated -= UpdateFluidColor;
        SmokeController.OnSmokeColorUpdated += UpdateFluidColor;
        crntImg = this.GetComponent<Image>();
        system = this.GetComponent<ParticleSystem>();
        UpdateFluidColor(ColorSelectionHandler.crntSelectedColor);

    }
    private void OnEnable()
    {
        crntImg = this.GetComponent<Image>();
        system = this.GetComponent<ParticleSystem>();
        UpdateFluidColor(ColorSelectionHandler.crntSelectedColor);
    }
    void UpdateFluidColor(Color color)
    {
        if (crntImg)
        {
            crntImg.color = color;
        }
        if (system)
        {
            system.startColor = color;
        }
    }
}
