using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class CharacterSoundController : MonoBehaviour
{

    public AudioClip[] NoNoSound, AngrySound, SmileSound,DialogSound;
    public static CharacterSoundController instance;
    AudioSource source;
    private void Awake()
    {
        instance = this;
        source=  this.GetComponent<AudioSource>();
    }
    public void PlayNoSound()
    {
        source.clip = NoNoSound[Random.Range(0, NoNoSound.Length)];
        source.Play();
    }
    public void PlayAngrySound()
    {
        source.clip = AngrySound[Random.Range(0, AngrySound.Length)];
        source.Play();
    }
    public void PlaySmileSound()
    {
        source.clip = SmileSound[Random.Range(0, SmileSound.Length)];
        source.Play();
    }
    public void PlayDialogSound()
    {
        source.clip = DialogSound[Random.Range(0, DialogSound.Length)];
        source.Play();
    }
}
