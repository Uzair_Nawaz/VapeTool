using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class LoadingPopup : MonoBehaviour
{
    public Image LoadingIMG;
    public GameObject PlayBtn;
    private void Start()
    {
        PlayBtn.SetActive(false);
        LoadingIMG.DOFillAmount(1, 15).OnComplete(() => {
            LoadingIMG.transform.parent.gameObject.SetActive(false);
            PlayBtn.SetActive(true);
        });
    }
}
