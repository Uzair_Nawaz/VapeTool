
using UnityEngine;
using UnityEngine.UI;

public class OutOfFluidPanel : MonoBehaviour
{
    [SerializeField] Button Coin;
    [SerializeField] Button Close;
    //private void Update()
    //{
    //    Coin.interactable = DefaultData.SharedData().EligbleForAd;
    //}
    private void OnEnable()
    {
        GameManager.SharedInstance().DisableSmokeParent();
    }
    void Start()
    {
        Coin.onClick.AddListener(() =>
        {
            AudioManager.SharedInstance().Play_Sound(Constants.ClickSound);
            //AdsManager.ShowRewardedVideo((AdCompleted) =>
            //{
            //    if (AdCompleted)
            //    {
                    GameManager.SharedInstance().EnableSmokeParent();
                    //GameManager.SharedInstance().current_vape.Fluid.fillAmount = 1;
                    gameObject.SetActive(false);
                    GameManager.Instance.SmokeHandler.isAlive = true;
                    GameManager.SharedInstance().Refill();
            //    }
            //    else
            //    {
            //        AdsManager.ShowInterstitial();
            //        GameManager.SharedInstance().EnableSmokeParent();
            //        //GameManager.SharedInstance().current_vape.Fluid.fillAmount = 1;
            //        gameObject.SetActive(false);
            //        GameManager.Instance.SmokeHandler.isAlive = true;
            //        GameManager.SharedInstance().Refill();
            //    }
            //});
            //    //AdsManager.Instance.ShowInterstitialAd();
            
        });

        Close.onClick.AddListener(() =>
        {
            GameManager.SharedInstance().EnableSmokeParent();
            gameObject.SetActive(false);
            //AdsManager.Instance.ShowInterstitialAd();
        });
    }
}
