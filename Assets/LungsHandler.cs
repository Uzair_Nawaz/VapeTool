using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Unity.VisualScripting;
using BitSplash.AI.GPT.Extras;

public class LungsHandler : MonoBehaviour
{
    Tween twen = null;
    public static bool isUserCough = false;
    public Image LungsImageContainer;
    static int count;
    static int multiple = 0;
    private void Start()
    {
        isUserCough = false;
        VapeController.OnDurationUpdate -= FillLungsVal;
        VapeController.OnDurationUpdate += FillLungsVal;

        UIAnimObjHandler.OnPageChange -= SetFillRate;
        UIAnimObjHandler.OnPageChange += SetFillRate;

    }
    private void OnDestroy()
    {
        VapeController.OnDurationUpdate -= FillLungsVal;
    }

    void FillLungsVal(float duration)
    {
        //Debug.Log("Duration" + duration);
        float dur = duration / 3;
        if (dur >= 1.1f)
        {
            AnimeGirlController.instance.PlayCharacterAnim(CharacterAnimationType.Angry);
            LungsImageContainer.material.SetFloat("_ShakeUvSpeed", 20);
            LungsImageContainer.material.SetFloat("_DistortAmount", 0.3f);
            isUserCough = true;
            VapeController.OnCoughStart?.Invoke();
        }
        if (duration <= 0)
        {
            try
            {
                RateMyApp.instance.CheckAndShowRateApp();
            }
            catch (System.Exception exc2)
            {

            }
            float EndSmokeDuration = 1.5f;
            if (isUserCough)
            {
                SoundManager.instance.StartCoughSound();

                EndSmokeDuration = 3;
            }
            else
            {
                if (LungsImageContainer.fillAmount > 0.1f)
                {
                    SoundManager.instance.StartExhaleSound(LungsImageContainer.fillAmount);
                    if (AnimeGirlController.instance)
                        AnimeGirlController.instance.PlayCharacterAnim(CharacterAnimationType.VapeEnd);
                }

            }
            twen = LungsImageContainer.DOFillAmount(0, LungsImageContainer.fillAmount * EndSmokeDuration).OnComplete(() =>
                {
                    twen = null;
                    LungsImageContainer.material.SetFloat("_ShakeUvSpeed", 0);
                    LungsImageContainer.material.SetFloat("_DistortAmount", 0);
                    count += 1;
                    if (count >= Random.Range(5 + multiple, 7 + multiple))
                    {
                        multiple += 2;
                        count = 0;
                        if (!UpdateCharacterOrientation.isCharacterSelectionScreenOpen)
                        {
                            CancelInvoke(nameof(SayMissMe));
                            Invoke(nameof(SayMissMe), 1);
                        }
                        UpdateCharacterOrientation.isCharacterSelectionScreenOpen = false;

                    }
                    if (isUserCough)
                    {
                        isUserCough = false;
                        GameManagerVape.isTouchEnable = true;
                        GameManagerVape.instance.CheckToShowFluidPopup();
                    }
                });


        }
        else
        {
            LungsImageContainer.fillAmount = dur;
        }
    }
    void SetFillRate()
    {
        if ((twen != null && !twen.IsPlaying()) || twen == null)
        {
            twen = LungsImageContainer.DOFillAmount(0, LungsImageContainer.fillAmount).OnComplete(() =>
            {
                LungsImageContainer.material.SetFloat("_ShakeUvSpeed", 0);
                LungsImageContainer.material.SetFloat("_DistortAmount", 0);
                if (isUserCough)
                {
                    isUserCough = false;
                    GameManagerVape.isTouchEnable = true;
                    GameManagerVape.instance.CheckToShowFluidPopup();
                }
            });
        }
    }
    void SayMissMe()
    {
        try
        {
            if (NPCFriend.instance)
                NPCFriend.instance.SayMissMeSentence();
            if (GameManagerVape.instance)
                GameManagerVape.instance.EnableDisableCharacterPanel(true);
        }
        catch (System.Exception exc)
        {

        }
    }
}
