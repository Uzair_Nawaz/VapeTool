using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AnyPortrait;
using Unity.VisualScripting;

public class Anime_Girl_Controller : MonoBehaviour
{
    public apPortrait apPortrait;
    public Collider2D headCollider;
    public Collider2D colliderBody;
    public Collider2D SkirtCollider;
    public Transform headPosition;


    private Vector2 lastEyeDirection= Vector2.zero;
    private Vector2 lastHeadDirection = Vector2.zero;
    private float lastBodyDir = 0.0f;
    private float dirTime = 0.0f;

    public AudioClip[] voice;
    public AudioSource audioSource;
    private apForceUnit forceLeft = null;

    private int angrySoundCount = 0; // Counter for angry sounds played in a row
    private int lastAngrySoundIndex = -1; // Index of the last played angry sound

    // Start is called before the first frame update
    void Start()
    {
        Invoke(nameof(startAnimation),1);
    }

    public void startAnimation()
    {

        if (!apPortrait.IsPlaying("Hi"))
        {
            apPortrait.Play("Hi");
            Invoke(nameof(hiSoundRandome),1f);
            apPortrait.CrossFadeQueued("Idle", 0.2f);
        }




        InvokeRepeating("playWind", 1f, Random.RandomRange(6, 8));
    }

    public void playWind()
    {
        if (Random.RandomRange(0, 99) > 50)
        {
            forceLeft = apPortrait.AddForce_Direction(new Vector2(1, 0)).SetPower(5000f);
            forceLeft.EmitOnce(5);
           
        }
        else
        {
            forceLeft = apPortrait.AddForce_Direction(new Vector2(-1, 0)).SetPower(5000.0f);
            forceLeft.EmitOnce(5);
           
        }

    }

    public void hiSoundRandome()
        
    {
        if (Random.RandomRange(0, 1) == 0)
        {
            PlaySound(0);
        } else
        {
            PlaySound(1);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hitResult = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hitResult.collider == headCollider)
            {
                if (!apPortrait.IsPlaying("Smile"))
                {
                    PlaySound(2);
                    apPortrait.CrossFade("Smile", 0.2f);
                    apPortrait.CrossFadeQueued("Idle");
                   
                }
               
            } else if (hitResult.collider == colliderBody)
            {
                if (!apPortrait.IsPlaying("Angry"))
                {

                    PlaySound(3);
                    apPortrait.CrossFade("Angry", 0.2f);
                    apPortrait.CrossFadeQueued("Idle");
                }

               

            }else if (hitResult.collider == SkirtCollider)
            {
                if (!apPortrait.IsPlaying("No No"))
                {
                    PlaySound(4);
                    apPortrait.CrossFade("No No", 0.2f);
                    apPortrait.CrossFadeQueued("Idle",0.1f);

                }
            }

            
        }
        if (Input.GetMouseButton(0))
        {
            Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            Vector2 eyeDir = mouseWorldPosition - headPosition.position;
            eyeDir.x = Mathf.Clamp(eyeDir.x, -10, 10)/10.0f;
            eyeDir.y = Mathf.Clamp(eyeDir.y, -10, 10) / 10.0f;

            Vector2 headDir = mouseWorldPosition - headPosition.position;
            headDir.x = Mathf.Clamp(headDir.x, -15, 15) / 15.0f;
            headDir.y = Mathf.Clamp(headDir.y, -15, 15) / 15.0f;

            float bodyDir = mouseWorldPosition.x - headPosition.position.x;
            bodyDir = Mathf.Clamp(eyeDir.x, -20, 20) / 20.0f;

            dirTime += Time.deltaTime;
            if (dirTime>0.3f)
            {
                dirTime = 0.3f;
            }
          


            apPortrait.SetControlParamVector2("Eye Direction", eyeDir,dirTime/0.3f);
            apPortrait.SetControlParamVector2("Head Direction", headDir, dirTime / 0.3f);
            apPortrait.SetControlParamFloat("Body Direction", bodyDir, dirTime / 0.3f);
            lastEyeDirection = eyeDir;
            lastHeadDirection = headDir;
            lastBodyDir = bodyDir;
        }else
        {
            dirTime -= Time.deltaTime;
            if (dirTime < 0.0f)
            {
                dirTime = 0.0f;

            }
            apPortrait.SetControlParamVector2("Eye Direction", lastEyeDirection, dirTime / 0.3f);
            apPortrait.SetControlParamVector2("Head Direction", lastHeadDirection, dirTime / 0.3f);
            apPortrait.SetControlParamFloat("Body Direction", lastBodyDir, dirTime / 0.3f);

        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            apPortrait.Play("vape Close");
           

            //  apPortrait.CrossFadeQueued("Vape Open", 0.2f);
            Debug.Log("Vape Close");
        }
        else if(Input.GetKeyUp(KeyCode.Space))
        {

            apPortrait.Play("Vape Open");
            apPortrait.CrossFadeQueued("Idle", 0.2f);
            Debug.Log("Vape Open");
           
        }
      
    }
    public void PlaySound(int index)
    {
        if (index >= 0 && index < voice.Length)
        {
            // Check if it's an angry sound and limit the consecutive plays
          
            audioSource.PlayOneShot(voice[index]);
        }
    }

   


}
