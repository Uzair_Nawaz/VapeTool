using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class WatchAVideo : MonoBehaviour
{
    [SerializeField]
    Button Watch;
    [SerializeField]
    Button Close;


    void Start()
    {
       
        Watch.onClick.AddListener(() =>
        {
            _WatchAVideo();
            GameManager.SharedInstance().EnableSmokeParent();
        });

        Close.onClick.AddListener(() =>
        {
            GameManager.SharedInstance().EnableSmokeParent();
            transform.DOScale(0, 0.5f);
            Invoke(nameof(ClosePopUp), 0.5f);
        });
    }

    private void ClosePopUp()
    {
        gameObject.SetActive(false);
        GameManager.SharedInstance().EnableSmokeParent();
        GameManager.SharedInstance().EnableSmokeParent();
    }

    public void _WatchAVideo()
    {
        AudioManager.SharedInstance().Play_Sound(Constants.ClickSound);
        Debug.Log("========> ad called");
        //AdsManager.ShowRewardedVideo((AdCompleted) =>
        //{
        //    Debug.Log("========>  AdCompleted "+ AdCompleted);
        //    if (AdCompleted)
        //    {
                Reward();
        //    }
        //    else
        //    {
        //        AdsManager.ShowInterstitial();
        //        Reward();
        //    }
        //});
    }
    void Reward()
    {
        
        GameManager.SharedInstance().GetCoinsForRewardedVideo();
        gameObject.SetActive(false);
        GameManager.SharedInstance().RefillPanel.SetActive(false);
        GameManager.SharedInstance().SmokeEnergyRefillPanel.SetActive(false);
        GameManager.Instance.TextScale.ScaleText();
        GameManager.SharedInstance().EnableSmokeParent();
    }
    private void OnEnable()
    {
        GameManager.SharedInstance().DisableSmokeParent();
        transform.DOScale(0, 0f).SetEase(Ease.Linear).OnComplete(() =>
        {
            transform.DOScale(1, 0.5f);
        });
    }
}
