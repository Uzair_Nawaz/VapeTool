using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioClip[] CoughSound;
    public AudioClip ThroatClearSound;
    public static SoundManager instance;
    public AudioSource VapeSource, ExhaleClipSource, CoughSource;
    private void Awake()
    {
        instance = this;
    }
    public void StartVapeSound()
    {
        if (VapeSource.isPlaying)
        {
            VapeSource.Stop();
        }
        VapeSource.Play();
    }
    public void StartCoughSound()
    {
        if (VapeSource.isPlaying)
        {
            VapeSource.Stop();
        }
        CoughSource.clip = CoughSound[Random.Range(0, CoughSound.Length)];
        StartCough();
        Invoke(nameof(StartCough), Random.Range(1.1f, 1.23f));
        Invoke(nameof(ClearThroat), Random.Range(1.5f, 1.75f));
    }
    public void CloseVapeSound()
    {
        if (VapeSource.isPlaying)
        {
            VapeSource.Stop();
        }
    }
    void StartCough()
    {
        CoughSource.Play();
    }
    void ClearThroat()
    {
        CoughSource.PlayOneShot(ThroatClearSound);
    }
    public void StartExhaleSound(float val)
    {
        if (VapeSource.isPlaying)
        {
            VapeSource.Stop();
        }
        float temp = 1 - val;
        ExhaleClipSource.pitch = 0.94f +temp;
        ExhaleClipSource.Play();
    }
}
