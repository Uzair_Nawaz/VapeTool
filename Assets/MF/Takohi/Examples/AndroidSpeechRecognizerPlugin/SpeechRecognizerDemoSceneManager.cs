﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Android;

public class SpeechRecognizerDemoSceneManager : MonoBehaviour
{
    private SpeechRecognizerManager _speechManager = null;
    private bool _isListening = false;
    public GameObject ListenerGO;
    public Text Status;
    public InputField QuestionStr;
    public GameObject StopRecordBtn, StartRecordBtn;
    #region MONOBEHAVIOUR
    const string RecordAudioStr = "android.permission.RECORD_AUDIO";
    void Start()
    {
        // We pass the game object's name that will receive the callback messages.
        _speechManager = new SpeechRecognizerManager(gameObject.name);
        if (Application.platform != RuntimePlatform.Android)
        {
            Debug.Log("Speech recognition is only available on Android platform.");
            return;
        }

        if (!SpeechRecognizerManager.IsAvailable())
        {
            Debug.Log("Speech recognition is not available on this device.");
            return;
        }
    }

    void OnDestroy()
    {
        if (_speechManager != null)
            _speechManager.Release();
    }

    #endregion

    #region SPEECH_CALLBACKS

    void OnSpeechEvent(string e)
    {
        switch (int.Parse(e))
        {
            case SpeechRecognizerManager.EVENT_SPEECH_READY:
                DebugLog("Ready for speech");
                enableRecordAnimBtn(true);
                break;
            case SpeechRecognizerManager.EVENT_SPEECH_BEGINNING:
                DebugLog("User started speaking");
                enableRecordAnimBtn(true);
                break;
            case SpeechRecognizerManager.EVENT_SPEECH_END:
                DebugLog("User stopped speaking");
                break;
        }
    }

    void OnSpeechResults(string results)
    {
        _isListening = false;

        // Need to parse
        string[] texts = results.Split(new string[] { SpeechRecognizerManager.RESULT_SEPARATOR }, System.StringSplitOptions.None);
        ;

        ListenerGO.SetActive(false);
        QuestionStr.text = texts[0];
        enableRecordAnimBtn(false);

        // _speechManager.StartListening (); // No parameters 
        DebugLog("Speech results:\n   " + string.Join("\n   ", texts));
    }

    void OnSpeechError(string error)
    {
        switch (int.Parse(error))
        {
            case SpeechRecognizerManager.ERROR_AUDIO:
                DebugLog("Error during recording the audio.");
                break;
            case SpeechRecognizerManager.ERROR_CLIENT:
                DebugLog("Error on the client side.");
                break;
            case SpeechRecognizerManager.ERROR_INSUFFICIENT_PERMISSIONS:
                DebugLog("Insufficient permissions. Do the RECORD_AUDIO and INTERNET permissions have been added to the manifest?");
                break;
            case SpeechRecognizerManager.ERROR_NETWORK:
                DebugLog("A network error occured. Make sure the device has internet access.");
                break;
            case SpeechRecognizerManager.ERROR_NETWORK_TIMEOUT:
                DebugLog("A network timeout occured. Make sure the device has internet access.");
                break;
            case SpeechRecognizerManager.ERROR_NO_MATCH:
                DebugLog("No recognition result matched.");
                break;
            case SpeechRecognizerManager.ERROR_NOT_INITIALIZED:
                DebugLog("Speech recognizer is not initialized.");
                break;
            case SpeechRecognizerManager.ERROR_RECOGNIZER_BUSY:
                DebugLog("Speech recognizer service is busy.");
                break;
            case SpeechRecognizerManager.ERROR_SERVER:
                DebugLog("Server sends error status.");
                break;
            case SpeechRecognizerManager.ERROR_SPEECH_TIMEOUT:
                DebugLog("No speech input.");
                break;
            default:
                break;
        }
        enableRecordAnimBtn(false);
        _isListening = false;
    }

    #endregion

    public void StartListening()
    {
        if (HasRecordingPermission())
        {
            if (!_isListening)
            {
                _isListening = true;
                _speechManager.StartListening(1, "en-US"); // Use english and return maximum three results.
                ListenerGO.SetActive(true);
                enableRecordAnimBtn(true);
                // _speechManager.StartListening (); // No parameters will use the device default language and returns maximum 5. results
            }
        }
        else
        {
            RequestRecordingPermission();
        }

    }
    void enableRecordAnimBtn(bool isEnable)
    {
        StopRecordBtn.SetActive(isEnable);
        StartRecordBtn.SetActive(!isEnable);
    }
    private bool HasRecordingPermission()
    {
#if !UNITY_EDITOR
#if UNITY_ANDROID
        using (var permissionChecker = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (var currentActivity = permissionChecker.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                var permissionStatus = currentActivity.Call<int>("checkSelfPermission", RecordAudioStr);
                return permissionStatus == 0; // 0 means PERMISSION_GRANTED
            }
        }
#endif
#else
        return true;
#endif
    }
    public void RequestRecordingPermission()
    {
#if !UNITY_EDITOR

        if (!HasRecordingPermission())
        {
            using (var permissionChecker = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                using (var currentActivity = permissionChecker.GetStatic<AndroidJavaObject>("currentActivity"))
                {
                    string[] permissions = { RecordAudioStr };
                    currentActivity.Call("requestPermissions", permissions, 0);
                }
            }
        }
#endif

    }

    public void StopListeninig()
    {
        if (_isListening)
        {
            _speechManager.StopListening();
            _isListening = false;
        }
    }
    public void CancelListening()
    {
        if (_isListening)
        {
            _speechManager.CancelListening();
            _isListening = false;
        }
        ListenerGO.SetActive(false);
        enableRecordAnimBtn(false);

    }

    #region DEBUG

    private void DebugLog(string message)
    {
        //Debug.Log(message);
        Status.text = message;
    }

    #endregion
}
