using System.Collections;
using System.Collections.Generic;
using AnyPortrait;
using UnityEngine;
public enum CharacterAnimationType { Hi, Idle, Smile, Angry, NoNo, VapeStart, VapeEnd }
public class AnimeGirlController : MonoBehaviour
{
    public apPortrait apPortrait;
    public static System.Action OnAnimeGirlChange;
    apForceUnit forceLeft = null;
    public AudioClip[] voice;
    AudioSource audioSource;
    Animator crntAnimator;
    public static AnimeGirlController instance;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        UpdatePortraitData();
        OnAnimeGirlChange -= UpdatePortraitData;
        OnAnimeGirlChange += UpdatePortraitData;
    }
    public void PlayCharacterAnim(CharacterAnimationType type)
    {
        crntAnimator.CrossFadeInFixedTime(type.ToString(), 0.1f);
    }
    public void playWind()
    {
        if (Random.Range(0, 99) > 50)
        {
            forceLeft = apPortrait.AddForce_Direction(new Vector2(1, 0)).SetPower(5000f);
            forceLeft.EmitOnce(5);

        }
        else
        {
            forceLeft = apPortrait.AddForce_Direction(new Vector2(-1, 0)).SetPower(5000.0f);
            forceLeft.EmitOnce(5);

        }

    }
    public void startAnimation()
    {

        //if (!apPortrait.IsPlaying("Hi"))
        //{
        //Debug.Log(apPortrait.GetAnimationPlayData("Hi").EndFrame);
        //apPortrait.Play("Hi");
        //Invoke(nameof(hiSoundRandome), 1f);
        //apPortrait.CrossFadeQueued("Idle", 0.2f);
        //}
    }

    //public void hiSoundRandome()

    //{
    //    if (Random.Range(0, 1) == 0)
    //    {
    //        PlaySound(0);
    //    }
    //    else
    //    {
    //        PlaySound(1);
    //    }

    //}
    public void PlaySound(int index)
    {
        if (index >= 0 && index < voice.Length)
        {
            // Check if it's an angry sound and limit the consecutive plays

            audioSource.PlayOneShot(voice[index]);
        }
    }
    private void OnDestroy()
    {
        OnAnimeGirlChange -= UpdatePortraitData;
    }
    void UpdatePortraitData()
    {
        apPortrait = GetComponentInChildren<apPortrait>();
        if (apPortrait)
        {
            crntAnimator = apPortrait.gameObject.GetComponent<Animator>();
            //apPortrait.AsyncInitialize();
            CancelInvoke(nameof(playWind));
            InvokeRepeating(nameof(playWind), 1f, Random.Range(6, 8));

        }
    }
}
