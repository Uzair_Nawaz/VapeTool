using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DetectMovmentDetection : MonoBehaviour
{
    public bool DetectTap;
    public CharacterAnimationType[] TapAnimation;
    private void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        if (DetectTap)
        {
            int range = Random.Range(0, TapAnimation.Length);
            AnimeGirlController.instance.PlayCharacterAnim(TapAnimation[range]);
            if (TapAnimation[range].ToString().Equals(CharacterAnimationType.Angry.ToString()))
            {
                CharacterSoundController.instance.PlayAngrySound();
            }
            if (TapAnimation[range].ToString().Equals(CharacterAnimationType.Hi.ToString()))
            {
                CharacterSoundController.instance.PlayDialogSound();
            }
            if (TapAnimation[range].ToString().Equals(CharacterAnimationType.NoNo.ToString()))
            {
                CharacterSoundController.instance.PlayNoSound();
            }
            if (TapAnimation[range].ToString().Equals(CharacterAnimationType.Smile.ToString()))
            {
                CharacterSoundController.instance.PlaySmileSound();

            }
        }
    }
}
