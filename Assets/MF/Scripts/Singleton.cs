#region Using

using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

#endregion

namespace SKS {
    public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T> {
        private static T _instance;

        [FormerlySerializedAs("_deactivateOnLoad")]
        [SerializeField, Tooltip("If set to true, the gameobject will deactivate on Awake")]
        private bool deactivateOnLoad;

        [FormerlySerializedAs("_dontDestroyOnLoad")]
        [SerializeField, Tooltip("If set to true, the singleton will be marked as \"don't destroy on load\"")]
        private bool dontDestroyOnLoad;

        private bool _isInitialized;

        private string activeSceneName;

        protected abstract void AddDependentComponents ();

        public static T Instance {
            get {
                if (_instance != null) {
                    return _instance;
                }

                var instances = Resources.FindObjectsOfTypeAll<T>();
                if (instances == null || instances.Length == 0) {
                    _instance = new GameObject(typeof(T).Name).AddComponent<T>();
                    _instance.AddDependentComponents();
                    return _instance;
                }

                _instance = instances.FirstOrDefault(i => i.gameObject.scene.buildIndex != -1);
                if (Application.isPlaying) {
                    _instance?.Init();
                }

                return _instance;
            }
        }

        protected virtual void Awake () {
            if (_instance == null || !_instance || !_instance.gameObject) {
                _instance = (T)this;
            }
            else if (_instance != this) {
                Debug.LogError($"Another instance of {GetType()} already exist! Destroying self...");
                Destroy(this.gameObject);
                return;
            }

            _instance.Init();
        }

        private void Init () {
            if (_isInitialized) {
                return;
            }

            if (dontDestroyOnLoad) {
                DontDestroyOnLoad(gameObject);
            }

            if (deactivateOnLoad) {
                gameObject.SetActive(false);
            }

            activeSceneName = SceneManager.GetActiveScene().name;

            SceneManager.sceneUnloaded += SceneManager_sceneUnloaded;

            InternalInit();
            _isInitialized = true;
        }

        private void SceneManager_sceneUnloaded(Scene scene)
        {
            if (!Instance || gameObject == null)
            {
                SceneManager.sceneUnloaded -= SceneManager_sceneUnloaded;
                _instance = null;
                return;
            }

            if (dontDestroyOnLoad)
            {
                return;
            }
            if (scene.name.Equals(activeSceneName))
            {
                SceneManager.sceneUnloaded -= SceneManager_sceneUnloaded;
                _instance = null;
            }
        }

        protected abstract void InternalInit ();

        private void OnApplicationQuit () {
            _instance = null;
        }

        protected virtual void OnDestroy () {
            SceneManager.sceneUnloaded -= SceneManager_sceneUnloaded;

            StopAllCoroutines();
            InternalOnDestroy();
            if (_instance != this) {
                return;
            }

            _instance = null;
            _isInitialized = false;
        }

        protected abstract void InternalOnDestroy ();
    }
}