using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum AnimationType { BirdAnimation, KissAnim, butterflyAnimation, LoveAnim, HeartAnimation, DestroySmoke, Default }
public class SmokeController : MonoBehaviour
{
    public static System.Action<Color> OnSmokeColorUpdated;
    public ParticleSystem NormalSmoke, SmokeForRound;
    public Animator SmokeAnimations, RoundAnim;
    public static SmokeController instance;
    public static System.Action<int> OnSmokeShapeUpdated;
    private void Awake()
    {
        instance = this;
    }
    public void ShowSimpleSmoke(float duration)
    {
        if ((!GameManagerVape.isTouchEnable && duration > 0.03f)|| NormalSmoke.isPlaying)
        {
            return;
        }
        bool ShowCirclePuff = false;
        int probability = 80;
        if (GameManagerVape.instance.CurrentSelectedSmoke != AnimationType.Default)
        {
            probability = 99;
        }
        if (duration > 1 && Random.Range(0, 100) <= probability)
        {
            ShowCirclePuff = true;
        }
        if (ShowCirclePuff)
        {
            if (GameManagerVape.instance.CurrentSelectedSmoke == AnimationType.Default)
            {
                ShowRoundSmoke();
            }
            else
            {
                ShowAdvanceSmoke(GameManagerVape.instance.CurrentSelectedSmoke);
            }
        }
        else
        {
            GameManagerVape.isTouchEnable = false;
            var main = NormalSmoke.main;
            main.duration = duration;
            NormalSmoke.gameObject.SetActive(true);
            NormalSmoke.Play();
            CancelInvoke(nameof(EnableVapePress));
            Invoke(nameof(EnableVapePress), duration);
        }

    }
    void EnableVapePress()
    {
        GameManagerVape.isTouchEnable = true;
        GameManagerVape.instance.CheckToShowFluidPopup();
    }
    public void ShowRoundSmoke()
    {
        if (!GameManagerVape.isTouchEnable)
        {
            return;
        }
        GameManagerVape.isTouchEnable = false;

        SmokeForRound.gameObject.SetActive(true);
        SmokeForRound.Play();
        StartCoroutine(ShowRound());

    }
    public void ShowAdvanceSmoke(AnimationType type)
    {
        if (!GameManagerVape.isTouchEnable)
        {
            return;
        }
        GameManagerVape.isTouchEnable = false;

        NormalSmoke.gameObject.SetActive(true);
        NormalSmoke.Play();
        StartCoroutine(ShowSmoke(type));
    }
    public IEnumerator ShowRound()
    {
        yield return Yielders.Get(0.5f);
        RoundAnim.gameObject.SetActive(true);
        RoundAnim.Play("RingSmoke");
    }
    public IEnumerator ShowSmoke(AnimationType type)
    {
        yield return Yielders.Get(1.5f);
        SmokeAnimations.gameObject.SetActive(true);
        SmokeAnimations.Play(type.ToString());
    }
}
