using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum OutlineType { TypeA, TypeB, TypeC, TypeD, TypeE }
[CreateAssetMenu(menuName = "BGDataContainer")]
public class BackgroundDataContainer : ScriptableObject
{
    public BGData[] allData;
    public OutlineType GetOutlineType(string BGid)
    {
        OutlineType data = new OutlineType();
        data = OutlineType.TypeA;
        foreach (var obj in allData)
        {
            if (obj.id.Equals(BGid))
            {
                data = obj.productImage;
                break;
            }
        }
        return data;
    }
}

[System.Serializable]
public class BGData
{
    public string id;
    public OutlineType productImage;
}
