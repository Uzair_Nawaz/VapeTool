using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class VapeRefillHandler : MonoBehaviour
{
    public Button WatchAdBtn, CoinBtn;
    public Text GameStr;
    public Image IconIMGToShow;
    public Sprite[] smokes;
    int type=6;
    private void Start()
    {
        WatchAdBtn.onClick.AddListener(() =>
        {
            refillVape();
        });
        CoinBtn.onClick.AddListener(() =>
        {
            refillVape();
        });

    }
    public void init(int index)
    {
        IconIMGToShow.gameObject.SetActive(true);
        IconIMGToShow.sprite = smokes[index];
        IconIMGToShow.SetNativeSize();
        GameStr.text = "Fill the juice...?";
        type = index;
    }
    public void ClosePopup()
    {
        Destroy(this.gameObject);
    }
    void refillVape()
    {
        GameManagerVape.instance.CurrentSelectedSmoke = (AnimationType)type;
        LiquidFluidHandler.instance.FillFluid();
        SmokeController.OnSmokeShapeUpdated?.Invoke(type);

        Destroy(this.gameObject);
    }
}
