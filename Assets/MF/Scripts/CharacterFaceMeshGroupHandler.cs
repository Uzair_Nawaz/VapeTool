using System.Collections;
using System.Collections.Generic;
using BitSplash.AI.GPT.Extras;
using Crosstales.RTVoice.Model;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

public class CharacterFaceMeshGroupHandler : MonoBehaviour
{
    public GameObject[] FaceCharacter;
    public BoxCollider2D MainCollider;
    public GameObject OtherColliders;
    public Crosstales.RTVoice.Model.VoiceAlias voices;
    public string Description;
    public static CharacterFaceMeshGroupHandler instance;

    private void Start()
    {
        instance = this;
        GameManagerVape.OnFaceShownStatusUpdate -= ShowHideFace;
        GameManagerVape.OnFaceShownStatusUpdate += ShowHideFace;


        CharacterController.OnCharacterPosSet -= UpdateColliderMesh;
        CharacterController.OnCharacterPosSet += UpdateColliderMesh;

        NPCFriend.instance.SayInitialSaying();
    }
    void UpdateColliderMesh(CharacterPos pos)
    {
        if (pos == CharacterPos.CenterPos)
        {
            MainCollider.enabled = false;
            OtherColliders.SetActive(true);
        }
        else
        {
            OtherColliders.SetActive(false);
            MainCollider.enabled = true;
        }
    }
    private void OnDestroy()
    {
        if (instance == this)
            instance = null;
        CharacterController.OnCharacterPosSet -= UpdateColliderMesh;
        GameManagerVape.OnFaceShownStatusUpdate -= ShowHideFace;
    }
    private void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        GameManagerVape.instance.EnableDisableCharacterPanel(true);
    }
    public void ShowHideFace(bool isHide)
    {
        foreach (GameObject item in FaceCharacter)
        {
            item.SetActive(isHide);
        }
    }
}
