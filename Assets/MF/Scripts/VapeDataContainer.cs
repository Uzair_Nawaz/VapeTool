using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "VapeDataContainer")]
public class VapeDataContainer : ScriptableObject
{
    public vapeData[] data;
    public int getMaxNumberOfVape(VapeType type)
    {
        int maxVal = 5;
        foreach (vapeData item in data)
        {
            if (item.id == type)
            {
                maxVal = item.maxNumber;
            }
        }
        return maxVal;
    }
}

[System.Serializable]
public class vapeData
{
    public VapeType id;
    public int maxNumber;
}
