#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Net;
using System;
using System.ComponentModel;
using UnityEditor.PackageManager;

public class DownloadSoundFromStoryBlock : EditorWindow
{

    string websiteURL = "https://example.com";
    string mp3Link = "";
    string downloadLocation = "C:/Downloads";
    bool downloading = false;
    float downloadProgress = 0f;
    string downloadStatus;
    [MenuItem("Window/FETCH AND DOWNLOAD AUDIO FROM STORY BLOCK")]
    static void Init()
    {
        DownloadSoundFromStoryBlock window = (DownloadSoundFromStoryBlock)EditorWindow.GetWindow(typeof(DownloadSoundFromStoryBlock));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Enter a URL From story Block to start process:");

        websiteURL = EditorGUILayout.TextField("URL:", websiteURL);
        // Allow the user to set the download location
        GUILayout.BeginHorizontal();
        GUILayout.Label("Download Location: " + downloadLocation);
        if (GUILayout.Button("Set Download Location"))
        {
            SetDownloadLocation();
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(20);
        if (GUILayout.Button("FetchAndDownload"))
        {
            Fetch();
        }

        GUILayout.Space(10);

        if (downloading)
        {
            GUILayout.Space(10);
            GUILayout.Label("Downloading:");
            Rect progressRect = GUILayoutUtility.GetRect(200, 20, "TextField");
            EditorGUI.ProgressBar(progressRect, downloadProgress, downloadStatus);
        }

    }

    void Fetch()
    {
        // Create a new WWW instance to fetch the webpage
        WWW www = new WWW(websiteURL);

        // Wait for the webpage to finish loading
        while (!www.isDone)
        {
            // Show a progress bar
            EditorUtility.DisplayProgressBar("Fetching HTML Source", "Loading " + websiteURL + "...", www.progress);
        }

        // Hide the progress bar
        EditorUtility.ClearProgressBar();

        // Check if there was an error loading the webpage
        if (string.IsNullOrEmpty(www.error))
        {
            // Find the second occurrence of ".mp3" in the HTML source
            string htmlSource = www.text;
            int firstIndex = htmlSource.IndexOf(".mp3");
            int secondIndex = htmlSource.IndexOf(".mp3", firstIndex + 1);

            // Check if a second occurrence was found
            if (secondIndex != -1)
            {
                // Extract the line that contains the second occurrence of ".mp3"
                int startIndex = htmlSource.LastIndexOf('"', secondIndex) + 1;
                int endIndex = htmlSource.IndexOf('"', secondIndex);

                mp3Link = htmlSource.Substring(startIndex, endIndex - startIndex);

                // Replace any backslashes with forward slashes in the download location
                mp3Link = mp3Link.Replace("\\", "");
                downloading = true;
                // Automatically start downloading the file
                Download();
            }
            else
            {
                Debug.LogError("Could not find a second occurrence of '.mp3' in the HTML source.");
            }
        }
        else
        {
            Debug.LogError("Error loading webpage: " + www.error);
        }
    }

    void Download()
    {
        // Make sure the download location ends with a forward slash
        if (!downloadLocation.EndsWith("/"))
        {
            downloadLocation += "/";
        }

        // Extract the filename from the MP3 link
        string filename = mp3Link.Substring(mp3Link.LastIndexOf('/') + 1);

        // Create a new WebClient instance to download the file
        WebClient client = new WebClient();

        // Set the download progress callback function
        client.DownloadProgressChanged -= OnDownloadProgressChanged;

        client.DownloadProgressChanged += OnDownloadProgressChanged;

        // Set the download completion callback function
        client.DownloadFileCompleted -= OnDownloadFileCompleted;

        client.DownloadFileCompleted += OnDownloadFileCompleted;

        // Start the download
        client.DownloadFileAsync(new Uri(mp3Link), downloadLocation + filename);

        // Set the download status to "Downloading"
        downloadStatus = "Downloading...";
    }

    // Callback function for the download progress
    void OnDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
    {
        // Update the download status with the progress percentage
        downloadStatus = $"Downloading... {e.ProgressPercentage}%";
    }

    // Callback function for the download completion
    void OnDownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
    {
        // Set the download status to "Download Complete" if there were no errors
        if (e.Error == null)
        {
            downloadStatus = "Download Complete";
        }
        // Otherwise, set the download status to "Download Failed"
        else
        {
            downloadStatus = "Download Failed";
        }
        downloading = false;
    }
    void SetDownloadLocation()
    {
        // Show the file picker dialog and wait for the user to select a directory
        string newLocation = EditorUtility.OpenFolderPanel("Select Download Location", downloadLocation, "");

        // If the user selected a directory, set the new download location
        if (!string.IsNullOrEmpty(newLocation))
        {
            downloadLocation = newLocation;
        }
    }


}
#endif