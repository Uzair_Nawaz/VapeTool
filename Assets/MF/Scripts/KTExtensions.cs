﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public static class KTExtensions {
    public static string ToKMB (this int num) {
        if (num > 999999999 || num < -999999999) {
            return num.ToString("0,,,.###B", CultureInfo.InvariantCulture);
        }
        else
        if (num > 999999 || num < -999999) {
            return num.ToString("0,,.##M", CultureInfo.InvariantCulture);
        }
        else
        if (num > 999 || num < -999) {
            return num.ToString("0,.#K", CultureInfo.InvariantCulture);
        }
        else {
            return num.ToString(CultureInfo.InvariantCulture);
        }
    }

    public static void AnchorsToCorners (this RectTransform t) {
        RectTransform pt = t.parent as RectTransform;

        if (t == null || pt == null) return;

        Vector2 newAnchorsMin = new Vector2(t.anchorMin.x + t.offsetMin.x / pt.rect.width,
                                            t.anchorMin.y + t.offsetMin.y / pt.rect.height);
        Vector2 newAnchorsMax = new Vector2(t.anchorMax.x + t.offsetMax.x / pt.rect.width,
                                            t.anchorMax.y + t.offsetMax.y / pt.rect.height);

        t.anchorMin = newAnchorsMin;
        t.anchorMax = newAnchorsMax;
        t.offsetMin = t.offsetMax = new Vector2(0, 0);
    }

    public static void FillScreen (this RectTransform rt) {
        rt.anchorMin = Vector2.zero;
        rt.anchorMax = Vector2.one;
        rt.sizeDelta = Vector2.zero;
    }

    public static AudioSource AddAudioSource (this GameObject gameObject, AudioClip clip, float volume, bool loop, bool playOnAwake, bool is3D, float maxDistance = 250f) {
        AudioSource source = gameObject.AddComponent<AudioSource>();
        source.clip = clip;
        source.volume = volume;
        source.loop = loop;
        source.rolloffMode = AudioRolloffMode.Linear;
        source.playOnAwake = playOnAwake;
        source.spatialBlend = is3D ? 1f : 0f;
        source.maxDistance = maxDistance;
        return source;
    }

    public static IEnumerable<T> Shuffle<T> (this IEnumerable<T> list) {
        var r = new System.Random((int)DateTime.Now.Ticks);
        var shuffledList = list.Select(x => new { Number = r.Next(), Item = x }).OrderBy(x => x.Number).Select(x => x.Item);
        return shuffledList.ToList();
    }

    public static void SnapTo (this ScrollRect scrollRect, RectTransform target) {
        Canvas.ForceUpdateCanvases();

        scrollRect.content.anchoredPosition =
            (Vector2)scrollRect.transform.InverseTransformPoint(scrollRect.content.position)
            - (Vector2)scrollRect.transform.InverseTransformPoint(target.position);
    }

    public static void DestroyAllChildren (this Transform transform, Type specificType = null) {
        if (transform == null) {
            return;
        }
        var children = new List<GameObject>();
        foreach (Transform child in transform) {
            if (child == null) {
                continue;
            }
            else if (specificType == null || child.GetComponent(specificType)) {
                children.Add(child.gameObject);
            }
        }
        children.ForEach(child => GameObject.Destroy(child));
    }

    private static System.Random rng = new System.Random();

    public static void Shuffle<T> (this IList<T> list) {
        int n = list.Count;
        while (n > 1) {
            n--;
            int k = ThreadSafeRandom.ThisThreadsRandom.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static class ThreadSafeRandom {
        [ThreadStatic] private static System.Random Local;

        public static System.Random ThisThreadsRandom {
            get { return Local ?? (Local = new System.Random(unchecked(Environment.TickCount * 31 + Thread.CurrentThread.ManagedThreadId))); }
        }
    }

    public static void ForceUpdateVerticalLayoutGroup (this RectTransform rectTransform) {
        Canvas.ForceUpdateCanvases();

        LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
        LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);

        VerticalLayoutGroup[] verticalLayoutGroups = rectTransform.GetComponentsInChildren<VerticalLayoutGroup>();
        for (int i = 0; i < verticalLayoutGroups.Length; i++) {
            RectTransform child = verticalLayoutGroups[i].GetComponent<RectTransform>();

            LayoutRebuilder.MarkLayoutForRebuild(child);
            LayoutRebuilder.ForceRebuildLayoutImmediate(child);
        }

        LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
        LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);

        Canvas.ForceUpdateCanvases();
    }

    public static void ForceUpdateHorizontalLayoutGroup (this RectTransform rectTransform) {
        Canvas.ForceUpdateCanvases();

        LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
        LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);

        HorizontalLayoutGroup[] layoutGroups = rectTransform.GetComponentsInChildren<HorizontalLayoutGroup>();
        for (int i = 0; i < layoutGroups.Length; i++) {
            RectTransform child = layoutGroups[i].GetComponent<RectTransform>();

            LayoutRebuilder.MarkLayoutForRebuild(child);
            LayoutRebuilder.ForceRebuildLayoutImmediate(child);
        }

        LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
        LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);

        Canvas.ForceUpdateCanvases();
    }

    public static byte[] ToByteArray (this Stream stream) {
        stream.Position = 0;
        byte[] buffer = new byte[stream.Length];
        for (int totalBytesCopied = 0; totalBytesCopied < stream.Length;)
            totalBytesCopied += stream.Read(buffer, totalBytesCopied, Convert.ToInt32(stream.Length) - totalBytesCopied);
        return buffer;
    }

    public static IEnumerable<TSource> DistinctBy<TSource, TKey> (this IEnumerable<TSource> source,
           Func<TSource, TKey> keySelector) {
        return source.DistinctBy(keySelector, null);
    }

    public static IEnumerable<TSource> DistinctBy<TSource, TKey> (this IEnumerable<TSource> source,
        Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));

        return _(); IEnumerable<TSource> _ () {
            var knownKeys = new HashSet<TKey>(comparer);
            foreach (var element in source) {
                if (knownKeys.Add(keySelector(element)))
                    yield return element;
            }
        }
    }

    public static int LayerMaskToLayer (this LayerMask layerMask) {
        int layerNumber = 0;
        int layer = layerMask.value;
        while (layer > 0) {
            layer = layer >> 1;
            layerNumber++;
        }
        return layerNumber - 1;
    }

    public static float GetSpeed (this Transform myTransform, Vector3 targetPoint, float movementSpeed) {
        return Mathf.Sqrt((Mathf.Pow((targetPoint.x - myTransform.position.x), 2) + Mathf.Pow((targetPoint.y - myTransform.position.y), 2)) / movementSpeed);
    }

    public static List<List<T>> GetAllCombos<T> (this List<T> list) {
        List<List<T>> result = new List<List<T>>();
        // head
        result.Add(new List<T>());
        result.Last().Add(list[0]);
        if (list.Count == 1)
            return result;
        // tail
        List<List<T>> tailCombos = GetAllCombos(list.Skip(1).ToList());
        tailCombos.ForEach(combo => {
            result.Add(new List<T>(combo));
            combo.Add(list[0]);
            result.Add(new List<T>(combo));
        });
        return result;
    }

    public static void SetLayerRecursively (this GameObject obj, string layerName) {
        if (null == obj) {
            return;
        }

        var newLayer = LayerMask.NameToLayer(layerName);

        obj.layer = newLayer;

        foreach (Transform child in obj.transform) {
            if (null == child) {
                continue;
            }
            SetLayerRecursively(child.gameObject, layerName);
        }
    }

    public static string ReplaceAt (this string input, int index, char newChar) {
        if (input == null) {
            throw new ArgumentNullException("input");
        }
        char[] chars = input.ToCharArray();
        chars[index] = newChar;
        return new string(chars);
    }
}
