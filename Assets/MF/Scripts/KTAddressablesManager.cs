using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.Networking;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.SceneManagement;

public class KTAddressablesManager : SKS.Singleton<KTAddressablesManager>
{

    private Dictionary<string, List<AsyncOperationHandle>> sceneDependentHandlers = new Dictionary<string, List<AsyncOperationHandle>>();
    public static System.Action OnAddressableInitialized;
    private List<AsyncOperationHandle> globalHandlers = new List<AsyncOperationHandle>();

    private AsyncOperationHandle initHandle;

    private async void Start()
    {
        SceneManager.sceneUnloaded -= OnSceneUnloaded;

        SceneManager.sceneUnloaded += OnSceneUnloaded;
        await SetupCatalog();
    }

    protected override void OnDestroy()
    {
        if (sceneDependentHandlers != null)
        {
            foreach (var pair in sceneDependentHandlers)
            {
                foreach (var item in pair.Value)
                {
                    if (item.IsValid())
                    {
                        Addressables.Release(item);
                    }
                }

                pair.Value.Clear();
            }

            sceneDependentHandlers.Clear();
        }

        if (globalHandlers != null)
        {
            foreach (var item in globalHandlers)
            {
                if (item.IsValid())
                {
                    Addressables.Release(item);
                }
            }
        }
        if (initHandle.IsValid())
        {
            Addressables.Release(initHandle);
        }

        SceneManager.sceneUnloaded -= OnSceneUnloaded;

        base.OnDestroy();
    }

    protected override void AddDependentComponents()
    {

    }

    protected override void InternalInit()
    {

    }

    protected override void InternalOnDestroy()
    {

    }

    private async Task SetupCatalog()
    {
        initHandle = Addressables.InitializeAsync(false);
        await initHandle.Task;

        HasInitAddressables = true;
        KTAddressablesManager.OnAddressableInitialized?.Invoke();
        print("KT Addressables init complete");
    }

    private void OnSceneUnloaded(Scene scene)
    {
        if (sceneDependentHandlers.ContainsKey(scene.name) && sceneDependentHandlers[scene.name] != null)
        {
            var toRemoveList = sceneDependentHandlers[scene.name];
            foreach (var item in toRemoveList)
            {
                if (item.IsValid())
                {
                    Addressables.Release(item);
                }
            }
            toRemoveList.Clear();

            sceneDependentHandlers.Remove(scene.name);
        }
    }

    private string GetCurrentScene()
    {
        return SceneManager.GetActiveScene().name;
    }

    private void CheckAndAddHandler(AsyncOperationHandle handle, bool isSceneDependent)
    {
        if (isSceneDependent)
        {
            var currentScene = GetCurrentScene();
            if (sceneDependentHandlers.ContainsKey(currentScene))
            {
                var existingList = sceneDependentHandlers[currentScene];
                existingList.Add(handle);
            }
            else
            {
                var newHandlers = new List<AsyncOperationHandle>
                {
                    handle
                };
                sceneDependentHandlers[currentScene] = newHandlers;
            }
        }
        else
        {
            globalHandlers.Add(handle);
        }
    }

    public async Task Initialize()
    {
        await SetupCatalog();
    }

    public async Task<AsyncOperationHandle<GameObject>?> InstantiateGameObject(AssetReference assetRef, bool isSceneDependent)
    {
        if (!assetRef.RuntimeKeyIsValid())
        {
            return null;
        }
        var handle = assetRef.InstantiateAsync();
        await handle.Task;

        if (handle.Task.Status == TaskStatus.RanToCompletion)
        {
            CheckAndAddHandler(handle, isSceneDependent);
            return handle;
        }
        else
        {
            Addressables.Release(handle);
            return null;
        }
    }

    public AsyncOperationHandle<GameObject>? InstantiateGameObjectSync(AssetReference assetRef, bool isSceneDependent)
    {
        if (!assetRef.RuntimeKeyIsValid())
        {
            return null;
        }
        var handle = assetRef.InstantiateAsync();
        handle.WaitForCompletion();

        if (handle.Task.Status == TaskStatus.RanToCompletion)
        {
            CheckAndAddHandler(handle, isSceneDependent);
            return handle;
        }
        else
        {
            Addressables.Release(handle);
            return null;
        }
    }

    public async Task<AsyncOperationHandle<GameObject>?> InstantiateGameObject(string assetKey, bool isSceneDependent)
    {

        var locationHandle = Addressables.LoadResourceLocationsAsync(assetKey);
        await locationHandle.Task;

        if (locationHandle.Task.Status == TaskStatus.RanToCompletion && locationHandle.Result.Count > 0)
        {
            var location = locationHandle.Result[0];

            var handle = Addressables.InstantiateAsync(location);
            await handle.Task;

            Addressables.Release(locationHandle);

            if (handle.Task.Status == TaskStatus.RanToCompletion)
            {
                CheckAndAddHandler(handle, isSceneDependent);
                return handle;
            }
            else
            {
                Addressables.Release(handle);
                return null;
            }
        }
        else
        {
            Addressables.Release(locationHandle);
            return null;
        }
    }

    public AsyncOperationHandle<GameObject>? InstantiateGameObjectSync(string assetKey, bool isSceneDependent)
    {

        var locationHandle = Addressables.LoadResourceLocationsAsync(assetKey);
        locationHandle.WaitForCompletion();

        if (locationHandle.Task.Status == TaskStatus.RanToCompletion && locationHandle.Result.Count > 0)
        {
            var location = locationHandle.Result[0];

            var handle = Addressables.InstantiateAsync(location);
            handle.WaitForCompletion();

            Addressables.Release(locationHandle);

            if (handle.Task.Status == TaskStatus.RanToCompletion)
            {
                CheckAndAddHandler(handle, isSceneDependent);
                return handle;
            }
            else
            {
                Addressables.Release(handle);
                return null;
            }
        }
        else
        {
            Addressables.Release(locationHandle);
            return null;
        }
    }

    public async Task<AsyncOperationHandle<T>?> LoadObject<T>(AssetReference assetRef, bool isSceneDependent) where T : Object
    {
        if (!assetRef.RuntimeKeyIsValid())
        {
            return null;
        }
        var handle = Addressables.LoadAssetAsync<T>(assetRef);
        await handle.Task;

        if (handle.Task.Status == TaskStatus.RanToCompletion)
        {
            CheckAndAddHandler(handle, isSceneDependent);
            return handle;
        }
        else
        {
            Addressables.Release(handle);
            return null;
        }
    }

    public async Task<AsyncOperationHandle<T>?> LoadObject<T>(string key, bool isSceneDependent) where T : Object
    {
        var locationHandle = Addressables.LoadResourceLocationsAsync(key);
        await locationHandle.Task;

        if (locationHandle.Task.Status == TaskStatus.RanToCompletion && locationHandle.Result.Count > 0)
        {
            var handle = Addressables.LoadAssetAsync<T>(locationHandle.Result[0]);
            await handle.Task;

            if (handle.Task.Status == TaskStatus.RanToCompletion)
            {
                CheckAndAddHandler(handle, isSceneDependent);
                Addressables.Release(locationHandle);
                return handle;
            }
            else
            {
                Addressables.Release(locationHandle);
                Addressables.Release(handle);
                return null;
            }
        }
        else
        {
            Addressables.Release(locationHandle);
            return null;
        }
    }

    public AsyncOperationHandle<T>? LoadObjectSync<T>(AssetReference assetRef, bool isSceneDependent) where T : Object
    {
        if (!assetRef.RuntimeKeyIsValid())
        {
            return null;
        }
        var handle = Addressables.LoadAssetAsync<T>(assetRef);
        handle.WaitForCompletion();

        if (handle.Task.Status == TaskStatus.RanToCompletion)
        {
            CheckAndAddHandler(handle, isSceneDependent);
            return handle;
        }
        else
        {
            Addressables.Release(handle);
            return null;
        }
    }

    public AsyncOperationHandle<T>? LoadObjectSync<T>(string key, bool isSceneDependent) where T : Object
    {
#if UNITY_EDITOR
        var loadHandle = Addressables.LoadAssetAsync<T>(key);
        
        loadHandle.WaitForCompletion();
        
        if (loadHandle.Task.Status == TaskStatus.RanToCompletion)
        {
            CheckAndAddHandler(loadHandle, isSceneDependent);
            return loadHandle;
        }
        else
        {
            Addressables.Release(loadHandle);
            return null;
        }
#endif
        var locationHandle = Addressables.LoadResourceLocationsAsync(key);
        locationHandle.WaitForCompletion();

        if (locationHandle.Task.Status == TaskStatus.RanToCompletion && locationHandle.Result.Count > 0)
        {
            var handle = Addressables.LoadAssetAsync<T>(locationHandle.Result[0]);
            handle.WaitForCompletion();

            if (handle.Task.Status == TaskStatus.RanToCompletion)
            {
                CheckAndAddHandler(handle, isSceneDependent);
                Addressables.Release(locationHandle);
                return handle;
            }
            else
            {
                Addressables.Release(locationHandle);
                Addressables.Release(handle);
                return null;
            }
        }
        else
        {
            Addressables.Release(locationHandle);
            return null;
        }
    }

    public async Task<AsyncOperationHandle<IList<T>>?> LoadList<T>(List<string> keys, bool isSceneDependent)
    {
#if UNITY_EDITOR
        var loadHandle = Addressables.LoadAssetsAsync<T>(keys, null, Addressables.MergeMode.Union);
        await loadHandle.Task;

        if (loadHandle.Task.Status == TaskStatus.RanToCompletion)
        {
            CheckAndAddHandler(loadHandle, isSceneDependent);
            return loadHandle;
        }
        else
        {
            Addressables.Release(loadHandle);
            return null;
        }
#endif
        var locationHandle = Addressables.LoadResourceLocationsAsync(keys, Addressables.MergeMode.Union);
        await locationHandle.Task;

        if (locationHandle.Task.Status == TaskStatus.RanToCompletion && locationHandle.Result.Count > 0)
        {
            var locations = locationHandle.Result;
            locations = locations.DistinctBy(l => l.PrimaryKey).ToList();

            var handle = Addressables.LoadAssetsAsync<T>(locations, null);
            await handle.Task;

            if (handle.Task.Status == TaskStatus.RanToCompletion)
            {
                CheckAndAddHandler(handle, isSceneDependent);
                Addressables.Release(locationHandle);
                return handle;
            }
            else
            {
                Addressables.Release(locationHandle);
                Addressables.Release(handle);
                return null;
            }
        }
        else
        {
            Addressables.Release(locationHandle);
            return null;
        }
    }

    public void ReleaseReference(AsyncOperationHandle handle)
    {
        try
        {
            if (handle.IsValid())
            {
                var currentSceneName = SceneManager.GetActiveScene().name;
                if (sceneDependentHandlers.ContainsKey(currentSceneName))
                {
                    var handlers = sceneDependentHandlers[currentSceneName];
                    if (handlers.Contains(handle))
                    {
                        handlers.Remove(handle);
                    }
                }
                else if (globalHandlers.Contains(handle))
                {
                    globalHandlers.Remove(handle);
                }
                Addressables.Release(handle);
            }
        }
        catch (System.Exception exc)
        {
            Debug.LogError(exc.Message);
        }
    }

    public async Task<bool> IsBundleDownloaded(string key)
    {
        var locationHandle = Addressables.LoadResourceLocationsAsync(key);
        await locationHandle.Task;

        if (locationHandle.Task.Status == TaskStatus.RanToCompletion && locationHandle.Result.Count > 0)
        {
            var location = locationHandle.Result[0];

            AsyncOperationHandle<long> getDownloadSize = Addressables.GetDownloadSizeAsync(location);
            await getDownloadSize.Task;

            if (getDownloadSize.Result > 0)
            {
                Addressables.Release(locationHandle);
                Addressables.Release(getDownloadSize);
                return false;
            }
            else if (getDownloadSize.Status == AsyncOperationStatus.Succeeded)
            {
                Addressables.Release(locationHandle);
                Addressables.Release(getDownloadSize);
                return true;
            }
            else
            {
                Addressables.Release(locationHandle);
                Addressables.Release(getDownloadSize);
                return false;
            }
        }
        else
        {
            Addressables.Release(locationHandle);
            return false;
        }
    }

    public bool IsBundleDownloadedSync(string key)
    {
        var locationHandle = Addressables.LoadResourceLocationsAsync(key);
        locationHandle.WaitForCompletion();

        if (locationHandle.Task.Status == TaskStatus.RanToCompletion && locationHandle.Result.Count > 0)
        {
            var location = locationHandle.Result[0];

            AsyncOperationHandle<long> getDownloadSize = Addressables.GetDownloadSizeAsync(location);
            getDownloadSize.WaitForCompletion();

            if (getDownloadSize.Result > 0)
            {
                Addressables.Release(locationHandle);
                Addressables.Release(getDownloadSize);
                return false;
            }
            else if (getDownloadSize.Status == AsyncOperationStatus.Succeeded)
            {
                Addressables.Release(locationHandle);
                Addressables.Release(getDownloadSize);
                return true;
            }
            else
            {
                Addressables.Release(locationHandle);
                Addressables.Release(getDownloadSize);
                return false;
            }
        }
        else
        {
            Addressables.Release(locationHandle);
            return false;
        }
    }

    public async Task DownloadDependencies(List<string> keys)
    {
        while (!HasInitAddressables)
        {
            await Task.Delay(1000);
        }
        var locationHandle = Addressables.LoadResourceLocationsAsync(keys, Addressables.MergeMode.Union);
        await locationHandle.Task;

        if (locationHandle.Task.Status == TaskStatus.RanToCompletion && locationHandle.Result.Count > 0)
        {
            var locations = locationHandle.Result;
            locations = locations.DistinctBy(l => l.PrimaryKey).ToList();

            var downloadHandle = Addressables.DownloadDependenciesAsync(locations, false);
            await downloadHandle.Task;

            Addressables.Release(downloadHandle);
            Addressables.Release(locationHandle);
        }
        else
        {
            Addressables.Release(locationHandle);
        }
    }

    public async Task ClearBundle(string key)
    {
        var handle = Addressables.ClearDependencyCacheAsync(key, false);
        await handle.Task;

        Addressables.Release(handle);
    }

    public async Task ClearBundles(List<string> keys)
    {
        var handle = Addressables.ClearDependencyCacheAsync(keys, false);
        await handle.Task;

        Addressables.Release(handle);
    }

    public bool HasInitAddressables
    {
        private set;
        get;
    } = false;
}
