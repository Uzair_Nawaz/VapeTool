using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class ChangeVapeOutline : MonoBehaviour
{
    public Material type1, type2, type3, type4, type5;
    Material Material;
    private void Start()
    {
        ChangeWallpaper.OnBGChange -= SetVapeOutline;
        ChangeWallpaper.OnBGChange += SetVapeOutline;
        SetVapeOutline();
        ChangeMaterialShinePosition();

    }
    void SetVapeOutline()
    {
        switch (ChangeWallpaper.GetOutlineType())
        {
            case OutlineType.TypeA:
                this.GetComponent<Image>().material = type1;
                break;
            case OutlineType.TypeB:
                this.GetComponent<Image>().material = type2;
                break;
            case OutlineType.TypeC:
                this.GetComponent<Image>().material = type3;
                break;
            case OutlineType.TypeD:
                this.GetComponent<Image>().material = type4;
                break;
            case OutlineType.TypeE:
                this.GetComponent<Image>().material = type5;
                break;
        }
        Material = this.GetComponent<Image>().material;
    }

    void ChangeMaterialShinePosition()
    {

        DOVirtual.Float(1, 0, 5, (value) => {
            Material.SetFloat("_ShineLocation", value);
        }).OnComplete(() => {
            ChangeMaterialShinePosition();
        });
    }
}
