using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SynthesisResponse
{
    public string sentence;
    public string audio;
}

public class TextToSpeechManager : MonoBehaviour
{
    string apiKey = "kGOdv5yl.oqUd2gMibwhBVp5C6ed57Wpxvs2LUKOW";
    public string baseUrl = "https://ariel-api.xandimmersion.com/tts/Linda";
    AudioSource source;
    public Dictionary<string, string[]> Options = new Dictionary<string, string[]>
    {
        { "Arabic",new string[] { "Farah", } },
        { "Chinese (Mandarin)",new string[] { "Daiyu", } },
        { "Danish",new string[] { "Emma", "Oscar", } },
        { "Dutch",new string[] { "Anke","Adriaan", } },
        { "English (Australian)",new string[] { "Mia","Grace","Jack", } },
        { "English (British)",new string[] { "Charlotte","Sophia","Elijah", } },
        { "English (Indian)",new string[] { "Advika","Onkar", } },
        { "English (New Zealand)",new string[] { "Ruby", } },
        { "English (South African)",new string[] { "Elna", } },
        { "English (US)",new string[] { "Mary","Linda","Patricia","Barbara","Susan","Paul","Michael","William","Thomas", } },
        { "English (Welsh)",new string[] { "Aeron", } },
        { "French",new string[] { "Capucine","Alix","Arnaud", } },
        { "French (Canadian)",new string[] { "Stephanie","Celine", } },
        { "German",new string[] { "Maria","Theresa","Felix", } },
        { "Hindi",new string[] { "Chhaya", } },
        { "Icelandic",new string[] { "Anna","Sigriour", } },
        { "Italian",new string[] { "Gabriella","Bella","Lorenzo", } },
        { "Japanese",new string[] { "Rika","Tanaka", } },
        { "Korean",new string[] { "Ji-Ho", } },
        { "Norwegian",new string[] { "Camilla", } },
        { "Polish",new string[] { "Katarzyna","Malgorzata","Piotr","Jan", } },
        { "Portuguese (Brazilian)",new string[] { "Tabata","Juliana","Pedro", } },
        { "Portuguese (European)",new string[] { "Pati","Adriano", } },
        { "Romanian",new string[] { "Alexandra", } },
        { "Russian",new string[] { "Inessa","Viktor", } },
        { "Spanish (European)",new string[] { "Francisca","Margarita","Mateo", } },
        { "Spanish (Mexican)",new string[] { "Leticia", } },
        { "Spanish (US)",new string[] { "Josefina","Rosa","Miguel", } },
        { "Swedish",new string[] { "Eva", } },
        { "Turkish",new string[] { "Mesut", } },
        { "Welsh",new string[] { "Angharad", } },
    };
    private void Start()
    {
        source = this.GetComponent<AudioSource>();
    }
  
    public void ConvertTextToAudio(string text)
    {
        StartCoroutine(SendRequest(text));
    }

    IEnumerator SendRequest(string text)
    {
        WWWForm form = new WWWForm();
        form.AddField("sentence", text);
        form.AddField("octave", "0");
        using (UnityWebRequest www = UnityWebRequest.Post(baseUrl, form))
        {
            www.SetRequestHeader("Authorization", "Api-Key " + apiKey);

            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError("Error While Sending: " + www.error);
            }
            else
            {
                SynthesisResponse response = JsonUtility.FromJson<SynthesisResponse>(www.downloadHandler.text);

                using (UnityWebRequest wwwAudio = UnityWebRequestMultimedia.GetAudioClip(response.audio, AudioType.WAV))
                {
                    wwwAudio.SetRequestHeader("x-requested-with", "http://127.0.0.1:8080");

                    wwwAudio.SendWebRequest();

                    while (!wwwAudio.isDone)
                    {
                        yield return null;
                    }

                    if (wwwAudio.isNetworkError)
                    {
                        Debug.LogError(wwwAudio.error);
                    }
                    else
                    {
                        AudioClip audioClip = DownloadHandlerAudioClip.GetContent(wwwAudio);
                        source.clip = audioClip;
                        source.Play();
                    }
                }
            }
        }
    }
}
