using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PopupType { SmokeRefillPopup , ColorSelectionPopup,NoInternetPopup, RatePopup }
public class PopupManager : MonoBehaviour
{
    public Transform PlaceToInstantiatePopup;
    public static PopupManager instance;
    private void Awake()
    {
        instance = this;
    }
    public GameObject InstantiatePopup(PopupType type)
    {
        try
        {
            GameObject obj = KTAddressablesManager.Instance.InstantiateGameObjectSync(type.ToString(), false).Value.Result;
            obj.transform.parent = PlaceToInstantiatePopup;
            obj.transform.localScale = Vector3.one;
            RectTransform rectTransform = obj.GetComponent<RectTransform>();
            rectTransform.anchorMin = Vector2.zero;
            rectTransform.anchorMax = Vector2.one;
            rectTransform.anchoredPosition = Vector2.zero;
            rectTransform.sizeDelta = Vector2.zero;
            obj.SetActive(true);
            return obj;
        }catch(System.Exception exc)
        {
            return null;
        }
    }
}
