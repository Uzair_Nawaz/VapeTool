using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class CharacterPositionHandler : MonoBehaviour
{
    public Vector3 SidePosition, CenterPosition;
    private void Start()
    {
        CharacterController.OnCharacterPosSet -= SetCharacterPosistion;
        CharacterController.OnCharacterPosSet += SetCharacterPosistion;
        
    }
    private void OnDestroy()
    {
        CharacterController.OnCharacterPosSet -= SetCharacterPosistion;
    }
    public void SetCharacterPosistion(CharacterPos pos)
    {
        if(pos == CharacterPos.CenterPos)
        {
            this.transform.DOLocalMove(CenterPosition, 0.5f);
        }
        if (pos == CharacterPos.SidePos)
        {
            this.transform.DOLocalMove(SidePosition, 0.5f);
        }
        if (pos == CharacterPos.OutOfCam)
        {
            this.transform.DOLocalMove(new Vector3(60, SidePosition.y, SidePosition.z), 1f);
        }
    }

}
