using System;
using System.Collections;
using System.Collections.Generic;
using BitSplash.AI.GPT.Extras;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.TextCore.Text;

public enum CharacterPos { CenterPos, SidePos, OutOfCam }
public enum Character { Char_1, Char_2, Char_3, Char_4 }
public class CharacterController : MonoBehaviour
{
    public static CharacterPos CrntPos = CharacterPos.SidePos;
    public static System.Action<CharacterPos> OnCharacterPosSet;
    public Transform character1Pos, character2Pos, character3Pos, character4Pos;
    GameObject? CharacterGO;
    bool isWaitingForPrevDownload = false;
    int index = 1;
    List<Character> AvailableChar = new List<Character>();
    private void Start()
    {
        if (KTAddressablesManager.Instance.HasInitAddressables)
        {
            StartGameFlow();
        }
        else
        {
            KTAddressablesManager.OnAddressableInitialized -= StartGameFlow;
            KTAddressablesManager.OnAddressableInitialized += StartGameFlow;
        }
    }
    void StartGameFlow()
    {
        DownloadServerCharacter();
        InstantiateCharacter(Character.Char_1);
    }

    public void NextBtnPress()
    {
        index++;
        if (index > AvailableChar.Count)
        {
            index = 1;
        }
        UpdateCharacter();
    }
    public void PreviousBtnPress()
    {
        index--;
        if (index <= 0)
        {
            index = AvailableChar.Count;
        }
        UpdateCharacter();
    }
    void UpdateCharacter()
    {
        switch (index)
        {
            case 1:
                InstantiateCharacter(Character.Char_1);
                break;
            case 2:
                InstantiateCharacter(Character.Char_2);
                break;
            case 3:
                InstantiateCharacter(Character.Char_3);
                break;
            case 4:
                InstantiateCharacter(Character.Char_4);
                break;
        }
        RateMyApp.instance.CheckAndShowRateApp();
        NPCFriend.instance.HideUIBTn();
    }
    public void InstantiateCharacter(Character type)
    {
        DownloadServerCharacter();
        if (CharacterGO)
        {
            Destroy(CharacterGO);
        }
       var CharHandler = KTAddressablesManager.Instance.InstantiateGameObjectSync(type.ToString(), false).Value;
        CharacterGO = (GameObject)CharHandler.Result;
        CharacterGO.transform.parent = GetPosToInstantiate(type);
        CharacterGO.transform.localPosition = Vector3.zero;
        CharacterGO.transform.localScale = Vector3.one;
        Invoke(nameof(FireCallBack), 0.1f);
    }
    void FireCallBack()
    {
        AnimeGirlController.OnAnimeGirlChange?.Invoke();
        OnCharacterPosSet?.Invoke(CrntPos);
    }
    Transform GetPosToInstantiate(Character type)
    {
        if (type == Character.Char_1)
        {
            return character1Pos;
        }
        if (type == Character.Char_2)
        {
            return character2Pos;
        }
        if (type == Character.Char_3)
        {
            return character3Pos;
        }
        if (type == Character.Char_4)
        {
            return character4Pos;
        }
        return character1Pos;
    }
    async void DownloadServerCharacter()
    {
        List<string> data = new List<string>();
        if (!AvailableChar.Contains(Character.Char_1))
        {
            AvailableChar.Add(Character.Char_1);
        }
        if (!AvailableChar.Contains(Character.Char_2))
        {
            AvailableChar.Add(Character.Char_2);
        }
        if (!await KTAddressablesManager.Instance.IsBundleDownloaded(Character.Char_3.ToString()))
        {
            data.Add(Character.Char_3.ToString());
        }
        else
        {
            if (!AvailableChar.Contains(Character.Char_3))
            {
                AvailableChar.Add(Character.Char_3);
            }
        }
        if (!await KTAddressablesManager.Instance.IsBundleDownloaded(Character.Char_4.ToString()))
        {
            data.Add(Character.Char_4.ToString());
        }
        else
        {
            if (!AvailableChar.Contains(Character.Char_4))
            {
                AvailableChar.Add(Character.Char_4);
            }
        }
        if (data.Count > 0)
        {
            if (!isWaitingForPrevDownload)
            {
                isWaitingForPrevDownload = true;
                await KTAddressablesManager.Instance.DownloadDependencies(data);
                isWaitingForPrevDownload = false;
            }



        }
    }
}
