using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.Video;
public class ChangeWallpaper : MonoBehaviour
{

    public static System.Action OnBGChange;
    int maxBG = 19;
    VideoPlayer player;
    AsyncOperationHandle handle;
   static int crntBGIndex;
    const string BGString = "Background_";
    const string BGData = "BGData";
    static BackgroundDataContainer BGDataHolder;
    private void Awake()
    {
        player = this.GetComponent<VideoPlayer>();
    }
    private void OnDestroy()
    {
        OnBGChange = null;
    }
    private void OnEnable()
    {
        StartVideo();
    }
    private void Start()
    {
        StartVideo();
    }
    private void StartVideo()
    {
        if (KTAddressablesManager.Instance.HasInitAddressables)
        {
            StartGameFlow();
        }
        else
        {
            KTAddressablesManager.OnAddressableInitialized -= StartGameFlow;
            KTAddressablesManager.OnAddressableInitialized += StartGameFlow;

        }
      
    }
    void StartGameFlow()
    {
        KTAddressablesManager.OnAddressableInitialized -= StartGameFlow;

        crntBGIndex = 1;
        ShowBG(crntBGIndex);
        DownloadServerBG();
    }
    public void ChangeBGRAndom()
    {
        crntBGIndex = getNextDownloadedBG(crntBGIndex);
        ShowBG(crntBGIndex);
    }
    void ShowBG(int index)
    {
        KTAddressablesManager.Instance.ReleaseReference(handle);

        handle = KTAddressablesManager.Instance.LoadObjectSync<VideoClip>(BGString + index.ToString(), false).Value;

        player.clip = handle.Result as VideoClip;
        player.Play();
        OnBGChange?.Invoke();
    }
    async void DownloadServerBG()
    {
        List<string> data = new List<string>();
        for (int i = 4; i <= maxBG; i++)
        {
            if (!await KTAddressablesManager.Instance.IsBundleDownloaded(BGString + i.ToString()))
            {
                data.Add(BGString + i.ToString());
            }
        }
        if (!await KTAddressablesManager.Instance.IsBundleDownloaded(BGData))
        {
            data.Add(BGData);
        }
        if (data.Count > 0)
        {
            await KTAddressablesManager.Instance.DownloadDependencies(data);
        }
        BGDataHolder = KTAddressablesManager.Instance.LoadObjectSync<BackgroundDataContainer>(BGData, false).Value.Result;
    }
    public static OutlineType GetOutlineType()
    {
        if (BGDataHolder)
        {
            return BGDataHolder.GetOutlineType(BGString + crntBGIndex);
        }
        else
        {
            return OutlineType.TypeA;
        }
    }
    int getNextDownloadedBG(int val)
    {
        val += 1;
        if (val > maxBG)
        {
            val = 0;
        }
        if (!KTAddressablesManager.Instance.IsBundleDownloadedSync(BGString + val.ToString()))
        {
            return getNextDownloadedBG(val);
        }
        else
        {
            return val;
        }
    }
    int getBeforeDownloadedBG(int val)
    {
        val -= 1;
        if (val <= 0)
        {
            val = maxBG;
        }
        if (!KTAddressablesManager.Instance.IsBundleDownloadedSync(BGString + val.ToString()))
        {
            return getBeforeDownloadedBG(val);
        }
        else
        {
            return val;
        }
    }

}
