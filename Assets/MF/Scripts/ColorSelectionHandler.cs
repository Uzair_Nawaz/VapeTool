using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSelectionHandler : MonoBehaviour
{
    public Color[] AllColors;
    public static Color32 crntSelectedColor;
  
    public void SetSmokeColor(int index)
    {
        crntSelectedColor = AllColors[index];
        SmokeController.OnSmokeColorUpdated?.Invoke(AllColors[index]);
        Destroy(this.gameObject);
    }
}
