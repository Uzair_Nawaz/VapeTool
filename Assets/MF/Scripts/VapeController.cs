using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class VapeController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    Coroutine vapeCor;
    public float Duration = 0;
    public static System.Action OnCoughStart;
    public static System.Action OnUserStartVape, OnUserEndVape;
    public static System.Action<float> OnDurationUpdate;
    public UnityEngine.UI.Image LungsData;
    private void Start()
    {
        OnCoughStart -= CoughStart;
        OnCoughStart += CoughStart;
    }
    void CoughStart()
    {
        OnUserReleaseVape(false);
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        if (!GameManagerVape.isTouchEnable || GameManagerVape.instance.CheckToShowFluidPopup() || LungsData.fillAmount > 0.05f)
        {
            if (LungsData.fillAmount > 0.05f)
            {
                UIAnimObjHandler.OnPageChange?.Invoke();
            }
            return;
        }
        LungsHandler.isUserCough = false;
        Debug.Log("DateTime" + System.DateTime.Now);
        CancelVapeCor();
        SoundManager.instance.StartVapeSound();
        UIAnimObjHandler.instance.ShowGamePlayAnim2();
        AnimeGirlController.instance.PlayCharacterAnim(CharacterAnimationType.VapeStart);
        vapeCor = StartCoroutine(Vaping());
        OnUserStartVape?.Invoke();
    }
    void CancelVapeCor()
    {
        SoundManager.instance.CloseVapeSound();
        if (vapeCor != null)
        {
            StopCoroutine(vapeCor);
        }
    }
    IEnumerator Vaping()
    {
        while (true)
        {
            Duration += Time.deltaTime;
            OnDurationUpdate?.Invoke(Duration);
            yield return null;
        }
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        OnUserReleaseVape(true);


    }
    void OnUserReleaseVape(bool ShouldShowSmoke)
    {
        try
        {
            OnUserEndVape?.Invoke();
            CancelVapeCor();
            if (!GameManagerVape.isTouchEnable || Duration <= 0)
            {
                return;
            }
            if (Duration > 2)
            {
                Duration = 2;
            }
            if (ShouldShowSmoke)
            {
                SmokeController.instance.ShowSimpleSmoke(Duration);
            }
            if (LungsData.fillAmount > 0.1f)
            {
                Duration = 0;
                OnDurationUpdate?.Invoke(Duration);
                GameManagerVape.instance.SaveFluid();
            }
            Duration = 0;
        }
        catch (System.Exception exc)
        {

        }
    }
}
