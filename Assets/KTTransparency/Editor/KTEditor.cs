﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using System.IO;

public class KTEditor
{
    const string k_TrackingDescription = "Your data will be used to provide you a better and personalized ad experience. And there are optional events that you can participate by registering with us.";
    [PostProcessBuild]
    public static void AddMissingKeys(BuildTarget buildTarget, string pathToBuiltProject)
    {
#if UNITY_IOS
        if (buildTarget == BuildTarget.iOS)
        {

            // Get plist
            string plistPath = pathToBuiltProject + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
            PlistElementDict rootDict = plist.root;

            var calenderKey = "NSCalendarsUsageDescription";
            rootDict.SetString(calenderKey, "Welcome");

            var cameraKey = "NSCameraUsageDescription";
            rootDict.SetString(cameraKey, "Welcome");

            var applovinSDKKey = "AppLovinSdkKey";
            rootDict.SetString(applovinSDKKey, "Hq6mTb8MLI85Z9JNXx_6wtaHPU5otk08X6v0i1N3JV8ueSGBNFHI7taWMfIDOJMwQFx7HIBbpJTSmld8-wQ5n_");
            rootDict.SetString("NSUserTrackingUsageDescription", k_TrackingDescription);
            // Write to file
            File.WriteAllText(plistPath, plist.WriteToString());

            string projectPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";

            PBXProject pbxProject = new PBXProject();
            pbxProject.ReadFromFile(projectPath);
            string target = pbxProject.TargetGuidByName("Unity-iPhone");

            pbxProject.AddBuildProperty(target, "OTHER_LDFLAGS", "-ObjC");
            pbxProject.AddBuildProperty(target, "OTHER_LDFLAGS", "-all_load");
            pbxProject.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

            //Write to file
            pbxProject.WriteToFile(projectPath);



        }
#endif
    }
}
