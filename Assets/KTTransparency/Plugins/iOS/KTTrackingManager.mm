#import <AppTrackingTransparency/AppTrackingTransparency.h>
#import <AdSupport/AdSupport.h>

extern "C" {
    void requestIDFA (const char *gameObjectName) {
        if (@available(iOS 14, *)) {
            NSString *callbackObjectName = [NSString stringWithUTF8String:gameObjectName];
            [ATTrackingManager requestTrackingAuthorizationWithCompletionHandler:^(ATTrackingManagerAuthorizationStatus status) {
                
                NSString *statusString=  @"";
                switch (status) {
                    case ATTrackingManagerAuthorizationStatusNotDetermined:
                        statusString = @"NotDetermined";
                        break;
                    case ATTrackingManagerAuthorizationStatusRestricted:
                        statusString = @"Restricted";
                        break;
                    case ATTrackingManagerAuthorizationStatusDenied:
                        statusString = @"Denied";
                        break;
                    case ATTrackingManagerAuthorizationStatusAuthorized:
                        statusString = @"Authorised";
                        break;;
                    default:
                        break;
                }
                if (callbackObjectName != nil) {
                    UnitySendMessage([callbackObjectName UTF8String], "OnAuthorisationChanged", [statusString UTF8String]);
                }
            }];
        } else {
            
        }
    }
}
