﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class KTTrackingManager : MonoBehaviour
{

#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern void requestIDFA (string gameObjectName);
#endif

    private static KTTrackingManager sharedInstance;
    private const string ATTpromptShownStatus = "@!#$%^#$%@^%$&%ATTpromptShownStatus^^%#$E%$@#WQ$#%5";

    private void Start()
    {
        if (sharedInstance == null)
        {
            sharedInstance = this;
            checkToShowATTPopopup();
            DontDestroyOnLoad(gameObject);
        }
        else if (sharedInstance != this)
        {
            Destroy(gameObject);
        }
    }
    void checkToShowATTPopopup()
    {
        if (ATTPromptAvailableToShow())
        {
            RequestIDFA();
        }
    }
    private static bool ATTPromptAvailableToShow()
    {
        if (!PlayerPrefs.HasKey(ATTpromptShownStatus))
        {
            return true;
        }
        return false;
    }

    private void OnDestroy()
    {
        if (sharedInstance && sharedInstance == this)
        {
            Destroy(sharedInstance);
            sharedInstance = null;
        }
    }

    /// Callback from native plugin isde
    private void OnAuthorisationChanged(string status)
    {
        //AnalyticsManager.LogIDFAStatusEvent(status);
    }

    private static float GetiOSVersion()
    {
#if UNITY_EDITOR || UNITY_ADNROID
        return -1;
#elif UNITY_IOS
		string systemVersion = UnityEngine.iOS.Device.systemVersion;
		string[] split = systemVersion.Split('.');

		if (split.Length > 1) {
			systemVersion = split[0] + "." + split[1];
		}

		return float.Parse(systemVersion, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
#else
		 return -1;
#endif
    }

    public static bool CanRequestIDFA()
    {
#if UNITY_IOS
#if UNITY_EDITOR
		return true;
#else
		return GetiOSVersion() >= 14;
#endif
#else
        return false;
#endif
    }

    public static void RequestIDFA()
    {
#if UNITY_IOS
		if (sharedInstance == null || sharedInstance.gameObject == null) {
			return;
		}
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			requestIDFA(sharedInstance.gameObject.name);
		}
#endif
    }
}
