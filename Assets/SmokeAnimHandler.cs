using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeAnimHandler : MonoBehaviour
{
    public void CloseAnim()
    {
        this.gameObject.SetActive(false);
        GameManagerVape.isTouchEnable = true;
        GameManagerVape.instance.CheckToShowFluidPopup();
    }
}
