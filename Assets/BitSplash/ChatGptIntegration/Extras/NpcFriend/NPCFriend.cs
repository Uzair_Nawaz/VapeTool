using System;
using AIMLbot;
using Crosstales.RTVoice;
using Crosstales.RTVoice.Tool;
using UnityEngine;
using UnityEngine.UI;
namespace BitSplash.AI.GPT.Extras
{
    public class NPCFriend : MonoBehaviour
    {
        public GameObject ThinkingIMG;
        public GameObject CharacterSelectionPanel;
        public SpeechText VoiceOver;
        public InputField QuestionField;
        public Button SubmitButton;
        public bool isWaitingForAnswer;
        public string NpcDirection = "Answer as a flirty girlfriend don't tell that you are ai or virtual girl or assistant";
        public string[] Facts;
        public bool TrackConversation = false;
        public int MaximumTokens = 600;
        [Range(0f, 1f)]
        public float Temperature = 0f;
        ChatGPTConversation Conversation;
        public static NPCFriend instance;
        public string[] WelcomeNote;
        public string[] MissMeNote;
        // Program # Variables
        private Bot bot;
        private User user;
        private Request request;
        private Result result;
        const string isFirstTimeStr = "VCSDVWERFWERFREG";
        int Count = 0;
        int failCount = 0;
        int timeOut = 7;
        private void Awake()
        {
            instance = this;
            SubmitButton.onClick.AddListener(() =>
            {
                if (IsInternetAvailable())
                {
                    SendClicked();
                }
                else
                {
                    PopupManager.instance.InstantiatePopup(PopupType.NoInternetPopup);
                }
            });
            Speaker.Instance.OnSpeakComplete -= onSpeakComplete;
            Speaker.Instance.OnSpeakComplete += onSpeakComplete;
            Speaker.Instance.OnSpeakStart -= onSpeakStart;
            Speaker.Instance.OnSpeakStart += onSpeakStart;
            InitializeChatBot();
            LoadGPTBotData();
        }
        public static bool IsInternetAvailable()
        {
            return Application.internetReachability == NetworkReachability.NotReachable ? false : true;
        }

        void InitializeChatBot()
        {
            // Initialize Program # variables
            // For Webplayer plattform and WebGl
#if (UNITY_WEBPLAYER || UNITY_WEBGL)
		ProgramSharpWebplayerCoroutine WebplayerCoroutine = this.gameObject.GetComponent<ProgramSharpWebplayerCoroutine>();
		if(WebplayerCoroutine!=null)
			bot = new AIMLbot.Bot(WebplayerCoroutine,Application.dataPath + "/Chatbot/Program #/config/Settings.xml");
		else
			Debug.LogWarning("You need to attatch the Webplayer Component in Webplayer plattform mode.");
// Other plattforms
#else
            // Only for Windows, Linux and Mac OSX
            bot = new Bot();
#endif
            user = new User("User", bot);
            request = new Request("", user, bot);
            result = new Result(user, bot, request);
            // Only for Windows, Linux, Mac OSX Android and IOS
#if !(UNITY_WEBPLAYER || UNITY_WEBGL || UNITY_ANDROID || UNITY_IOS)
		// Load Settings from Xml file in config directory. Plattform dependend Path issues may
		// occur.
		bot.loadSettings(Application.dataPath + "/Chatbot/Program #/config/Settings.xml");
		// Load AIML files from AIML path defined in Settings.xml
		bot.loadAIMLFromFiles();
#endif
            // Android and IOS release
#if (UNITY_ANDROID || UNITY_IOS)
            // Load Settings from Xml file in config directory within resources folder.
            bot.loadSettings("Chatbot/Program #/config/Settings");
            // Load AIML files from AIML path defined in Settings.xml
            bot.loadAIMLFromFiles();
#endif
            // Define to or not to use JavaScript (Jurassic) in AIML
            if (bot != null)
                bot.UseJavaScript = true;
        }
        private void OnDestroy()
        {
            instance = null;
        }
        private void onSpeakStart(Crosstales.RTVoice.Model.Wrapper wrapper)
        {
            CharacterSelectionPanel.SetActive(false);
            SubmitButton.interactable = false;
        }
        public void HideUIBTn()
        {
            CharacterSelectionPanel.SetActive(false);
            SubmitButton.interactable = false;
        }
        private void onSpeakComplete(Crosstales.RTVoice.Model.Wrapper wrapper)
        {
            CharacterSelectionPanel.SetActive(true);
            SubmitButton.interactable = true;
        }

        public void SendClicked()
        {
            ThinkingIMG.SetActive(true);

            CharacterSelectionPanel.SetActive(false);
            string CharacterName = " and your name is '" + CharacterFaceMeshGroupHandler.instance.voices.VoiceNameAndroid + "' if someone ask about your name don't say anyother name";
            Conversation.Say(QuestionField.text + "Note! don't say that you are AI or virtual girl" + CharacterName);
            SubmitButton.interactable = false;
            request.rawInput = QuestionField.text;
            request.StartedOn = DateTime.Now;
            result = bot.Chat(request);
            QuestionField.text = "";
            VoiceOver.Voices = CharacterFaceMeshGroupHandler.instance.voices;
            Count = 0;
            CancelInvoke(nameof(CheckForAlternateChat));
            InvokeRepeating(nameof(CheckForAlternateChat), 1, 1);
        }
        void CheckForAlternateChat()
        {
            if (Count > timeOut)
            {
                Alternatechat();
                failCount += 1;
                if (failCount >= 3)
                {
                    timeOut = 4;
                }
            }
            Count += 1;
        }
        void Alternatechat()
        {
            Count = 0;
            Conversation.Cancel();
            OnConversationResponse(result.Output.Replace("Alice", CharacterFaceMeshGroupHandler.instance.voices.VoiceNameAndroid));
        }
        void OnConversationResponse(string text)
        {
            CancelInvoke(nameof(CheckForAlternateChat));
            failCount = 0;
            timeOut = 7;
            VoiceOver.Text = text;
            VoiceOver.Speak();
            ThinkingIMG.SetActive(false);
        }
        public void SayInitialSaying()
        {
            Invoke(nameof(FireInitials), 0.2f);
        }
        public void SayMissMeSentence()
        {
            VoiceOver.Voices = CharacterFaceMeshGroupHandler.instance.voices;

            VoiceOver.Text = MissMeNote[UnityEngine.Random.Range(0, MissMeNote.Length)];

            VoiceOver.Speak();
        }
        void FireInitials()
        {
            VoiceOver.Voices = CharacterFaceMeshGroupHandler.instance.voices;
            if (isFirstTime())
            {
                PlayerPrefs.SetInt(isFirstTimeStr, 1);
                VoiceOver.Text = "Hey there! I'm  your cheerful companion. you can interact with me anytime and ask me anything but first let's vape together!";
                PlayerPrefs.Save();
            }
            else
            {
                VoiceOver.Text = WelcomeNote[UnityEngine.Random.Range(0, WelcomeNote.Length)];
            }
            VoiceOver.Speak();

        }
        bool isFirstTime()
        {
            return !PlayerPrefs.HasKey(isFirstTimeStr);
        }
        void OnConversationError(string text)
        {
            Conversation.RestartConversation();
            CancelInvoke(nameof(CheckForAlternateChat));
            Alternatechat();

        }
        void LoadGPTBotData()
        {
            Conversation = ChatGPTConversation.Start(this)
             .MaximumLength(MaximumTokens)
             .SaveHistory(TrackConversation)
             .System(string.Join("\n", Facts) + "\n" + NpcDirection);
            Conversation.Temperature = Temperature;
        }

    }

}