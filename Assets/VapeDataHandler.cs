using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.Video;

public enum VapeType { Mods_, SigaLike_, Pens_, Pods_, Disposable_, Shisha_ }

public class VapeDataHandler : MonoBehaviour
{
    public AssetReferenceGameObject BtnTemp;
    public Transform BtnPlaceToInstantiate;
    VapeDataContainer vapeData;
    const string VapeDataStr = "VapeDataStr";
    List<AsyncOperationHandle> allHandler = new List<AsyncOperationHandle>();
    private void Start()
    {
        if (KTAddressablesManager.Instance.HasInitAddressables)
        {
            StartGameFlow();
        }
        else
        {
            KTAddressablesManager.OnAddressableInitialized -= StartGameFlow;
            KTAddressablesManager.OnAddressableInitialized += StartGameFlow;
        }
    }
    void StartGameFlow()
    {
        KTAddressablesManager.OnAddressableInitialized -= StartGameFlow;
        DownloadServerVapeData();
    }
    void ClearOldData()
    {
        foreach (AsyncOperationHandle item in allHandler)
        {
            KTAddressablesManager.Instance.ReleaseReference(item);

        }
    }
    public void InstantantiateVapeData(int EnumIndex)
    {
        ClearOldData();
        AsyncOperationHandle handle = new AsyncOperationHandle();
        VapeType type = (VapeType)EnumIndex;// Enum.Parse(typeof(VapeType), EnumIndex.ToString());
        int MaxNumb = 3;
        if (vapeData)
        {
            MaxNumb = vapeData.getMaxNumberOfVape(type);
        }
        else
        {
            DownloadServerVapeData();
        }
        for (int i = 1; i <= MaxNumb; i++)
        {

            handle = KTAddressablesManager.Instance.LoadObjectSync<Sprite>(type.ToString() + i.ToString(), false).Value;
            Sprite spr = handle.Result as Sprite;
            allHandler.Add(handle);

            handle = KTAddressablesManager.Instance.InstantiateGameObjectSync(BtnTemp, false).Value;
            GameObject Obj =(GameObject)handle.Result;
            allHandler.Add(handle);
            Obj.transform.parent = BtnPlaceToInstantiate;
            Obj.transform.localScale = Vector3.one;
            Obj.GetComponent<vapeBtnData>().initVape(spr);
        }
    }
    void DownloadServerVapeData()
    {
        vapeData = KTAddressablesManager.Instance.LoadObjectSync<VapeDataContainer>(VapeDataStr, false).Value.Result;
        if (vapeData)
        {
            DownloadServerVape(VapeType.Mods_);
            DownloadServerVape(VapeType.SigaLike_);
            DownloadServerVape(VapeType.Pens_);
            DownloadServerVape(VapeType.Pods_);
            DownloadServerVape(VapeType.Disposable_);
            //DownloadServerVape(VapeType.Shisha);
        }
    }
    async void DownloadServerVape(VapeType Enum)
    {
        List<string> data = new List<string>();
        int maxNumb = vapeData.getMaxNumberOfVape(Enum);
        for (int i = 4; i <= maxNumb; i++)
        {
            if (!await KTAddressablesManager.Instance.IsBundleDownloaded(Enum.ToString() + i.ToString()))
            {
                data.Add(Enum.ToString() + i.ToString());
            }
        }
        if (data.Count > 0)
        {
            await KTAddressablesManager.Instance.DownloadDependencies(data);
        }
    }
}
