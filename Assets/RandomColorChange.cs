using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColorChange : MonoBehaviour
{
    ParticleSystem Particle;
    public Color[] AllColors;
    // Start is called before the first frame update
    void Start()
    {
        SmokeHandler.OnSmokeStart -= ChangeSmokeColor;
        SmokeHandler.OnSmokeStart += ChangeSmokeColor;
        Particle = this.GetComponent<ParticleSystem>();
   
    }
    private void OnDestroy()
    {
        SmokeHandler.OnSmokeStart -= ChangeSmokeColor;
    }
    void ChangeSmokeColor()
    {
        var main = Particle.main;
        main.startColor = AllColors[Random.Range(0, AllColors.Length)];
    }
}
