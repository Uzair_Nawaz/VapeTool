using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using DeadMosquito.AndroidGoodies;
using JetBrains.Annotations;
using AndroidGoodiesExamples;
using System.Linq;

public class GameManagerVape : MonoBehaviour
{
    public static System.Action<bool> OnFaceShownStatusUpdate;// call this method when enable camera
    public AnimationType CurrentSelectedSmoke = AnimationType.Default;
    public Image VapeImg, ZoomVapeImg;
    public static GameManagerVape instance;
    public static bool isTouchEnable = true;
    public Material SelectedMaterial, UnSelectedMaterial;
    public Material ringMaterial;
    public GameObject VapePanel, CharacterPanel;
    public Image LoadingScreen;
    Coroutine cor;
    public static bool isGameStarted = false;
    private const string CAMERA_PERMISSION = "android.permission.CAMERA";

    [SerializeField] GameObject bg, arCamera, popupPanel;

    private void Awake()
    {
        instance = this;
        isTouchEnable = true;
        UpdateCharacterOrientation.isCharacterSelectionScreenOpen = false;
    }
    private void Start()
    {
        updateSmokeColor();
        SmokeController.OnSmokeColorUpdated -= SetMaterialColor;
        SmokeController.OnSmokeColorUpdated += SetMaterialColor;
        VapeController.OnUserStartVape -= StartFlash;
        VapeController.OnUserStartVape += StartFlash;

        VapeController.OnUserEndVape -= StopCor;
        VapeController.OnUserEndVape += StopCor;

    }
    void SetMaterialColor(Color color)
    {
        ringMaterial.color = color;
    }
    public void StartGamePlay(Sprite sprite)
    {
        VapeImg.sprite = sprite;
        ZoomVapeImg.sprite = sprite;
        VapeImg.SetNativeSize();
        UIAnimObjHandler.instance.ShowGamePanel();
        OnRequestPermissions();

    }
    public void OnRequestPermissions()
    {
        // Don't forget to also add the permissions you need to manifest!
        var permissions = new[]
        {
        AGPermissions.CAMERA
    };

        // Filter permissions so we don't request already granted permissions,
        // otherwise if the user denies already granted permission the app will be killed
        var nonGrantedPermissions = permissions.ToList().Where(x => !AGPermissions.IsPermissionGranted(x)).ToArray();

        if (nonGrantedPermissions.Length == 0)
        {
            Debug.Log("User already granted all these permissions: " + string.Join(",", permissions));
            return;
        }

        // Finally request permissions user has not granted yet and log the results
        AGPermissions.RequestPermissions(permissions, results =>
        {
            // Process results of requested permissions
            foreach (var result in results)
            {
                Debug.Log(string.Format("Permission [{0}] is [{1}], should show explanation?: {2}",
                    result.Permission, result.Status, result.ShouldShowRequestPermissionRationale));
                if (result.Status == AGPermissions.PermissionStatus.Denied)
                {
                    // User denied permission, now we need to find out if he clicked "Do not show again" checkbox
                    if (result.ShouldShowRequestPermissionRationale)
                    {
                        // User just denied permission, we can show explanation here and request permissions again
                        // or send user to settings to do so
                    }
                    else
                    {
                        // User checked "Do not show again" checkbox or permission can't be granted.
                        // We should continue with this permission denied
                    }
                }
            }
        });
    }

    void StartFlash()
    {
        StopCor();
        if (UIAnimObjHandler.isProMode)
        {
            return;
        }
        cor = StartCoroutine(ToggleFlashLight());
    }
    void StopCor()
    {
        if (cor != null)
        {
            StopCoroutine(cor);
        }
        OnFlashlightOff();

    }
    IEnumerator ToggleFlashLight()
    {
        while (true)
        {
            OnFlashlightOn();
            yield return Yielders.Get(0.07f);
            OnFlashlightOff();
            yield return Yielders.Get(0.02f);

        }
    }

    public void OnFlashlightOn()
    {
        if (AGFlashLight.HasFlashlight() && AGPermissions.IsPermissionGranted(AGPermissions.CAMERA))
        {
            AGFlashLight.Enable();
        }
    }

    public void OnFlashlightOff()
    {
        if (AGFlashLight.HasFlashlight() && AGPermissions.IsPermissionGranted(AGPermissions.CAMERA))
        {
            AGFlashLight.Disable();
        }
    }

    public void SaveFluid()
    {
        PlayerPrefs.SetFloat(CurrentSelectedSmoke.ToString(), LiquidFluidHandler.instance.GetFluidVal());
        PlayerPrefs.Save();
    }
    public bool CheckToShowFluidPopup()
    {
        bool flag = LiquidFluidHandler.instance.GetFluidVal() <= 0;
        if (flag)
        {
            SmokeShapeHandler.UserPressSmokeShapeBtn(6, false);
        }
        return flag;
    }
    public void StopProMode()
    {
        OnFaceShownStatusUpdate?.Invoke(true);
        bg.SetActive(true);
        arCamera.SetActive(false);
    }
    public bool StartProMode()
    {
        if (HasCameraPermission() == true)
        {
            OnFaceShownStatusUpdate?.Invoke(false);
            arCamera.SetActive(true);
            bg.SetActive(false);
            return true;
        }
        else
        {
            popupPanel.SetActive(true);

            try
            {
                RequestCameraPermission();

            }
            catch (Exception exc)
            {
            }
            return false;
        }


    }
    private bool HasCameraPermission()
    {
#if !UNITY_EDITOR
#if UNITY_ANDROID
        using (var permissionChecker = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (var currentActivity = permissionChecker.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                var permissionStatus = currentActivity.Call<int>("checkSelfPermission", CAMERA_PERMISSION);
                return permissionStatus == 0; // 0 means PERMISSION_GRANTED
            }
        }
#endif
#else
        return true;
#endif
    }

    public void RequestCameraPermission()
    {
#if !UNITY_EDITOR

        if (!HasCameraPermission())
        {
            using (var permissionChecker = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                using (var currentActivity = permissionChecker.GetStatic<AndroidJavaObject>("currentActivity"))
                {
                    string[] permissions = { CAMERA_PERMISSION };
                    currentActivity.Call("requestPermissions", permissions, 0);
                }
            }
        }
#endif

    }

    public void EnableDisableCharacterPanel(bool enable)
    {
        if (UIAnimObjHandler.isProMode || !isGameStarted)
        {
            return;
        }
        LoadingScreen.DOFade(1, 0.5f).OnComplete(() =>
        {
            CharacterPanel.SetActive(enable);
            VapePanel.SetActive(!enable);
            LoadingScreen.DOFade(0, 0.5f);
        });
    }
    void updateSmokeColor()
    {
        ColorSelectionHandler.crntSelectedColor = Color.white;
        SmokeController.OnSmokeColorUpdated?.Invoke(Color.white);
        SetMaterialColor(Color.white);
    }
    public void ShowColorSelectionPopup()
    {
        PopupManager.instance.InstantiatePopup(PopupType.ColorSelectionPopup);
    }
}
