﻿// =========================
// MAKAKA GAMES - MAKAKA.ORG
// =========================

using UnityEngine;
using UnityEngine.UI;

[HelpURL("https://makaka.org/unity-assets")]
[AddComponentMenu ("AR/CameraAsBackground")]
public class CameraAsBackground : MonoBehaviour 
{
	private RawImage rawImage;
	private WebCamTexture webCamTexture;
	private AspectRatioFitter aspectRatioFitter;

	private float minimumWidthForOrientation = 100;
	private float EulerAnglesOfPI = 180f;
	
	private Rect uvRectForVideoVerticallyMirrored = new Rect(1f, 0f, -1f, 1f);	
	private Rect uvRectForVideoNotVerticallyMirrored = new Rect(0f, 0f, 1f, 1f);

	private float currentCWNeeded;
	private float currentAspectRatio;

	private Vector3 currentLocalEulerAngles = Vector3.zero;
	public static bool isDone;

   


    void Awake()
	{
		aspectRatioFitter = GetComponent<AspectRatioFitter>();
		rawImage = GetComponent<RawImage>();

		try
		{
			 isDone=Application.RequestUserAuthorization(UserAuthorization.WebCam).isDone;

			if (Application.platform != RuntimePlatform.Android)
			{
				while (!isDone)
				{
					isDone = Application.RequestUserAuthorization(UserAuthorization.WebCam).isDone;
					Debug.Log("isCamera -->" + isDone);
				}
			}
			if (isDone)
			{


				if (WebCamTexture.devices.Length == 0)
				{
					Debug.LogWarning("No 🎥 cameras found");
				}
				else
				{
					// Get Main Camera == Back Camera
					webCamTexture = new WebCamTexture(
						WebCamTexture.devices[0].name,
						Screen.width,
						Screen.height,
						30);

					Play();

					rawImage.texture = webCamTexture;
				}
			}
		}
		catch (System.Exception e)
		{
			Debug.LogWarning("Camera 🎥 is not available: " + e);

        }
	}
    private void OnEnable()
    {
		Play();
    }
    private void OnDisable()
    {
		Stop();
    }
	
    void Update ()
	{
		SetOrientationUpdate();
		
		//Test Texture Sizes
		//print(webCamTexture.width + ", " + webCamTexture.height);
	}

	public void SetOrientationUpdate()
	{
		if (webCamTexture)
		{
			if (webCamTexture.width < minimumWidthForOrientation) 
			{
				return;
			}

			currentCWNeeded = -webCamTexture.videoRotationAngle;

			if (webCamTexture.videoVerticallyMirrored) 
			{
				currentCWNeeded += EulerAnglesOfPI;
			}

			currentLocalEulerAngles.z = currentCWNeeded;
			rawImage.rectTransform.localEulerAngles =  currentLocalEulerAngles;

			currentAspectRatio = (float) webCamTexture.width / (float) webCamTexture.height;
			aspectRatioFitter.aspectRatio = currentAspectRatio;

			if (webCamTexture.videoVerticallyMirrored) 
			{
				rawImage.uvRect =  uvRectForVideoVerticallyMirrored;
			}
			else
			{
				rawImage.uvRect =  uvRectForVideoNotVerticallyMirrored;
			}
		}
	}

	public WebCamTexture GetWebCamTexture()
	{
		return webCamTexture;
	}

	public void Play()
	{
		if (webCamTexture)
		{
			webCamTexture.Play();
		}
	}

	public void Stop()
	{
		if (webCamTexture)
		{
			webCamTexture.Stop();
		}
	}

	public void ChangeResolutionAndPlay(float factor)
	{
		Stop();

		webCamTexture.requestedWidth = Mathf.RoundToInt(webCamTexture.requestedWidth * factor);
		webCamTexture.requestedHeight = Mathf.RoundToInt(webCamTexture.requestedHeight * factor);
		
		Play();
	}

	void OnDestroy()
	{
		Stop();
	}
}