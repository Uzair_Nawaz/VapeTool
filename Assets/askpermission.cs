using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class askpermission : MonoBehaviour
{
    private const string CAMERA_PERMISSION = "android.permission.CAMERA";

    public Text permissionStatusText;

    private bool HasCameraPermission()
    {
        using (var permissionChecker = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (var currentActivity = permissionChecker.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                var permissionStatus = currentActivity.Call<int>("checkSelfPermission", CAMERA_PERMISSION);
                return permissionStatus == 0; // 0 means PERMISSION_GRANTED
            }
        }
    }

    public void RequestCameraPermission()
    {
        if (!HasCameraPermission())
        {
            using (var permissionChecker = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                using (var currentActivity = permissionChecker.GetStatic<AndroidJavaObject>("currentActivity"))
                {
                    string[] permissions = { CAMERA_PERMISSION };
                    currentActivity.Call("requestPermissions", permissions, 0);
                }
            }
        }
        else
        {
            permissionStatusText.text = "Camera permission already granted!";
        }
    }
}

