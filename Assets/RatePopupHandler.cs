using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatePopupHandler : MonoBehaviour
{
   public void NotNow()
    {
        Destroy(this.gameObject);
    }
    public void RateNow()
    {
        NotNow();
        RateMyApp.instance.RateAndReview();
    }
}
