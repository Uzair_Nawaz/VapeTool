using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class LiquidFluidHandler : MonoBehaviour
{
    public Image LungsImageContainer;
    bool isCompleted = true;
    public static LiquidFluidHandler instance;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        VapeController.OnDurationUpdate -= ConsumeFluid;
        VapeController.OnDurationUpdate += ConsumeFluid;
        SmokeController.OnSmokeColorUpdated -= UpdateFluidColor;
        SmokeController.OnSmokeColorUpdated += UpdateFluidColor;

    }
    void UpdateFluidColor(Color color)
    {
        LungsImageContainer.color = color;
    }
    private void OnDestroy()
    {
        VapeController.OnDurationUpdate -= ConsumeFluid;
    }
    public float GetFluidVal()
    {
        return LungsImageContainer.fillAmount;
    }
    public void FillFluid(float val=0)
    {
        if (val == 0)
        {
            val = 1;
        }
        LungsImageContainer.DOFillAmount(val, 0.5f).OnComplete(() =>
        {
            isCompleted = true;
        });
    }
    void ConsumeFluid(float duration)
    {
        float dur = duration / 3;
        if (LungsImageContainer.fillAmount > 0 && duration > 0 && dur <= 1.1f && isCompleted)
        {
            isCompleted = false;
            LungsImageContainer.DOFillAmount(LungsImageContainer.fillAmount-GetSmokeConsumptionVal(), 0.1f).OnComplete(() =>
            {
                isCompleted = true;
            });
        }
    }
    float GetSmokeConsumptionVal()
    {
        float val = 0.0075f;
        switch (GameManagerVape.instance.CurrentSelectedSmoke)
        {
            case AnimationType.Default:
                val = 0.005f;
                break;
        }
        return val;
    }
}
