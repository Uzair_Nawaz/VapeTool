using UnityEngine;
using UnityEngine.UI;

public class CoinsCounter : MonoBehaviour
{
    [SerializeField] public Text CoinsText;
    void Start()
    {
        if (!PlayerPrefs.HasKey(Constants.CoinsKey))
        {
            PlayerPrefs.SetInt(Constants.CoinsKey, 200);
        }
        UpdateCoinsText();
    }

    public void AddCoins(int coins)
    {
        if (!PlayerPrefs.HasKey(Constants.CoinsKey))
        {
            PlayerPrefs.SetInt(Constants.CoinsKey, coins);
        }
        else
        {
            int current_coins = PlayerPrefs.GetInt(Constants.CoinsKey);
            PlayerPrefs.SetInt(Constants.CoinsKey, coins + current_coins);
        }
        UpdateCoinsText();
    }

    public bool DeductCoins(int coins)
    {
        if (!PlayerPrefs.HasKey(Constants.CoinsKey))
        {
            //Show amount exhausted panel
            GameManager.SharedInstance().WatchAVideo.SetActive(true);
            UpdateCoinsText();
            return false;
        }
        else
        {
            int current_coins = PlayerPrefs.GetInt(Constants.CoinsKey);
            if(current_coins < coins)
            {
                //Show amount exhausted panel
                GameManager.SharedInstance().WatchAVideo.SetActive(true);
                GameManager.SharedInstance().watchVideoCoinsToAward = 200;
                UpdateCoinsText();
                return false;
            }
            else
            {
                int netCoins = current_coins - coins;
                PlayerPrefs.SetInt(Constants.CoinsKey, netCoins);
                UpdateCoinsText();
                return true;
            }
        }
    }

    public void UpdateCoinsText()
    {
        if (!PlayerPrefs.HasKey(Constants.CoinsKey))
        {
            CoinsText.text = "0";
        }
        else
        {
            int current_coins = PlayerPrefs.GetInt(Constants.CoinsKey);
            CoinsText.text = current_coins.ToString();
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            PlayerPrefs.SetInt(Constants.CoinsKey, 500000);
            UpdateCoinsText();
        }
    }

}
