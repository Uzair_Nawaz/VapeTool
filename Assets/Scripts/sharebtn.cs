using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class sharebtn : MonoBehaviour
{
    public GameObject[] ToggleGO;
    List<GameObject> ActivatedGO=new List<GameObject>();
    void updateList()
    {
        ActivatedGO.Clear();
        foreach (var item in ToggleGO)
        {
            if (item.activeInHierarchy)
            {
                ActivatedGO.Add(item);
            }
        }
    }
    void toggleGO(bool isOn)
    {
        foreach (var item in ActivatedGO)
        {
            item.SetActive(isOn);
        }
    }
    public void share()
    {
        updateList();
        toggleGO(false);
        StartCoroutine(TakeScreenshotAndShare());
    }
    public void Capture()
    {
        SaveScreenShot();
    }
    void SaveScreenShot()
    {
        SSAndSave(() => {
            toggleGO(true);
        });
    }
    void SSAndSave(System.Action CallBack)
    {
        updateList();
        toggleGO(false);
        StartCoroutine(TakeScreenshotAndSave(CallBack));
    }
    private IEnumerator TakeScreenshotAndSave(System.Action CallBack)
    {
        yield return new WaitForEndOfFrame();

        Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();

        // Save the screenshot to Gallery/Photos
        string Name = "Image" + System.DateTime.Now.ToString() + ".png";
        Name = Name.Replace("/", "");
        Name = Name.Replace("-", "");
        Name = Name.Replace(".", "");
        Name = Name.Replace(":", "");
        Name = Name.Replace(" ", "");
        Name = Name.Replace(",", "");
        blink.SetActive(true);
        yield return new WaitForEndOfFrame();
        blink.SetActive(false);
        NativeGallery.Permission permission = NativeGallery.SaveImageToGallery(ss, "ScreenShot", Name, (success, path) => Debug.Log("Media save result: " + success + " " + path));
        CallBack?.Invoke();
        Destroy(ss);
       

    }
    public GameObject blink;
    private IEnumerator TakeScreenshotAndShare()
    {
        string timeStamp = System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss");
        yield return new WaitForEndOfFrame();
        Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();
        string filePath = Path.Combine(Application.temporaryCachePath, "shared img.png");
        File.WriteAllBytes(filePath, ss.EncodeToPNG());
        blink.SetActive(true);
        yield return new WaitForEndOfFrame();
        blink.SetActive(false);
        Destroy(ss);
        toggleGO(true);
        new NativeShare().AddFile(filePath)
            .SetSubject("Play Amazing Vape Puff Game").SetText("Vape Puff").SetUrl(GetGameURL())
            .SetCallback((result, shareTarget) => Debug.Log("Share result: " + result + ", selected app: " + shareTarget))
            .Share();
    }

    public static string GetGameURL()
    {
        string GameLink = "";
#if UNITY_ANDROID
        GameLink = "market://details?id=com.vape.coil.smock.smokegrenade.cigarrate.blunt.roller.ball.simulation";
#endif
#if UNITY_IOS
        GameLink = " https://itunes.apple.com/app/id " + "com.sks.virtual.vape.pod.trick.simulator.asmr.app";
#endif
        return GameLink;
    }
}
