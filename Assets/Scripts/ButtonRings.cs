using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonRings : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Image InhaleBtnImage;
    [SerializeField] Sprite onImage;
    [SerializeField] Sprite offImage;
    [SerializeField] Sprite BlueBarImage;
    [SerializeField] Sprite RedBarImage;
    [SerializeField] Image FillImage;
    [SerializeField] public Image RingsEnergyFillImage;
    [SerializeField] List<ParticleSystem> SmokeRings;
    public System.Action OnSmokeEnergyBarEmpty;
    private float smokeDuration;
    //private Slider slider;
    private Inhaler Inhaler;
    private bool CanExhaleSmoke;
    private AudioSource InhaleAS;
    private AudioSource ExhaleAS;
    bool isPressed;

    void Start()
    {
        CanExhaleSmoke = true;
        //slider = GameManager.SharedInstance().slider;
        Inhaler = GameManager.SharedInstance().Inhaler;
        Inhaler.InhaleProgressImage.fillAmount = 0;
        OnSmokeEnergyBarEmpty -= OnSmokeBarEmpty;
        OnSmokeEnergyBarEmpty += OnSmokeBarEmpty;
    }

    void OnSmokeBarEmpty()
    {
        GameManager.Instance.SmokeEnergyRefillPanel.SetActive(true);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        smokeDuration = Inhaler.InhaleProgressImage.fillAmount * 100;
        isPressed = false;
        InhaleBtnImage.sprite = offImage;
        StartCoroutine(UnfillSlider());
        InhaleAS.Stop();
        if (CanExhaleSmoke)
        {
            ExhaleAS = AudioManager.SharedInstance().PlayExhaleSound();
            GameManager.SharedInstance().coinsCounter.AddCoins(Convert.ToInt32(smokeDuration / 10));
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if(GameManager.SharedInstance().current_vape.Fluid.fillAmount > 0)
        {
            if(RingsEnergyFillImage.fillAmount > 0)
            {
                if (FillImage.fillAmount > 0)
                {
                    isPressed = true;
                    InhaleBtnImage.sprite = onImage;
                    StartCoroutine(ConsumeJuice());
                    StartCoroutine(FillSlider());
                    InhaleAS = AudioManager.SharedInstance().PlayInhaleSound();
                }
                else
                {
                    InhaleBtnImage.sprite = offImage;
                }
            }
            else
            {
               GameManager.SharedInstance().SmokeEnergyRefillPanel.SetActive(true);
            }
        }
        else
        {
            GameManager.SharedInstance().RefillPanel.SetActive(true);
        }
    }

    IEnumerator ConsumeJuice()
    {
        while (isPressed && FillImage.fillAmount > 0)
        {
            yield return new WaitForEndOfFrame();
            FillImage.fillAmount -= Time.deltaTime * 0.1f;
            if (FillImage.fillAmount <= 0)
                GameManager.SharedInstance().RefillPanel.SetActive(true);
        }

    }

    IEnumerator FillSlider()
    {
        while (isPressed)
        {
            yield return new WaitForEndOfFrame();
            Inhaler.InhaleProgressImage.fillAmount += (Time.deltaTime * 50) / 100;
            if (RingsEnergyFillImage.fillAmount > 0)
            {
                RingsEnergyFillImage.fillAmount -= Time.deltaTime / 6;
            }
            else
            {
                OnSmokeEnergyBarEmpty?.Invoke();
            }
            if (Inhaler.InhaleProgressImage.fillAmount >= 1)
            {
                StartCoroutine(ExecuteDelay());
            }
        }
    }

    IEnumerator ExecuteDelay()
    {
        yield return new WaitForSeconds(0.3f);
        if (isPressed)
        {
            isPressed = false;
            StartCoroutine(UnfillSliderWithoutSmoke());
        }
            
    }

    IEnumerator UnfillSliderWithoutSmoke()
    {
        CanExhaleSmoke = false;
        AudioSource aSource = AudioManager.SharedInstance().Play_Sound(Constants.CoughSound);
        while (!(isPressed) && Inhaler.InhaleProgressImage.fillAmount > 0 && !CanExhaleSmoke)
        {
            yield return new WaitForEndOfFrame();
            Inhaler.InhaleProgressImage.fillAmount -= (Time.deltaTime * 50) / 100;
            Inhaler.InhaleProgressImage.sprite = RedBarImage;
            if (Inhaler.InhaleProgressImage.fillAmount <= 0)
            {
                CanExhaleSmoke = true;
                Inhaler.InhaleProgressImage.sprite = BlueBarImage;
            }
                
        }
    }

    IEnumerator UnfillSlider()
    {
        float TimeElapsed = 0;
        while (!(isPressed) && Inhaler.InhaleProgressImage.fillAmount > 0 && CanExhaleSmoke)
        {
            yield return new WaitForEndOfFrame();
            TimeElapsed += Time.deltaTime;
            Inhaler.InhaleProgressImage.fillAmount -= (Time.deltaTime * 25) / 100;
            float WaitTime = UnityEngine.Random.Range(0.6f,1f);
            if(TimeElapsed > WaitTime)
            {
                PlaySmoke();
                TimeElapsed = 0;
            }
        }
    }

    void PlaySmoke()
    {
        ParticleSystem particle = null;
        SmokeRings.ForEach((p) =>
        {
            if (!p.isPlaying && !particle)
            {
                particle = p;
            }

        });

        if (particle && GameManager.SharedInstance().current_vape.Fluid.fillAmount >= 0)
        {
            particle.Play();
            particle = null;
            //UnfillSliderWithoutSmoke();
        }
    }
}
