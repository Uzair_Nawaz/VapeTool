using UnityEngine;
using UnityEngine.UI;

public class Inhaler : MonoBehaviour
{
    public Image InhaleProgressImage;
    private void OnEnable()
    {
        InhaleProgressImage.fillAmount = PlayerPrefs.GetFloat(GameManager.RingsEnergyFillImageValueKey, 1);
    }
}
