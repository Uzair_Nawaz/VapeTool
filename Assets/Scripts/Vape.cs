using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Uzair { }

public class Vape : MonoBehaviour
{
    public Image VapeTank;
    public Image VapeBattery;
    public Image Fluid;
    public Button BtnInhale;


    private void Start()
    {
        SetVapeAccessories();
    }

    public void SetVapeAccessories()
    {
        string TankId = PlayerPrefs.GetString(Constants.CurrentTank, "Tnk_def");
        string BatteryId = PlayerPrefs.GetString(Constants.CurrentBattery, "Bat_def");

        Sprite CurrentTank = GameManager.SharedInstance().AllShopAccessories.GetItemById(TankId).productImage;
        Sprite CurrentBattery = GameManager.SharedInstance().AllShopAccessories.GetItemById(BatteryId).productImage;

        VapeTank.sprite = CurrentTank;
        VapeBattery.sprite = CurrentBattery;
    }

    
}
