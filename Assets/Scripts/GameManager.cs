using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using DG.Tweening;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public static bool appOpenToShow;
    //public Slider slider;
    public Inhaler Inhaler;
    public GameObject Shop;
    public Vape current_vape;
    public ParticleSystem smoke, grow;
    public GameObject RefillPanel;
    public GameObject SmokeEnergyRefillPanel;
    public Image SmokeRingsButton;
    public GameObject WatchAVideo;
    public CoinsCounter coinsCounter;
    public GameObject ItemPurchasePopup;
    public ShopItem currently_purchased_item;
    public static GameManager Instance { get; private set; }
    public GameObject HealthSlider;
    [SerializeField]
    private Sprite[] bgImages;
    [SerializeField]
    private GameObject currentBg;
    [SerializeField]
    private GameObject smokeParticleParent;
    [SerializeField]
    private GameObject[] ObjsToDeactiveForScreenshot;

    [SerializeField]
    private GameObject nativeAdPrefab;
    [SerializeField]
    private GameObject loadingPanel;

    public GameObject popupPanel;

    public int watchVideoCoinsToAward = 50;
    public bool PermissionGranterd;
    ///////////////////////////////////////////////////////////////////////////

    [System.Serializable]
    public class SmokeTypeIconsState
    {
        public Image SelectedIcon;
        public Sprite DefaultIcon;
        public Sprite SmokeDefault;

        public Image iconInPanel;
        public SmokeTypes SmokeType;                                        //  Button States
        public Sprite ActiveState;
        public Sprite UnactiveState;
    }
    public List<SmokeTypeIconsState> TypeIconsStates;

    [SerializeField] GameObject[] shopButtons;
    [SerializeField] GameObject coinIcon;


    //[SerializeField] public Color[] smokesColors;

    [SerializeField] GameObject bg, modeBtn, arCamera;
    [SerializeField] Sprite[] modeBg;
    bool isPro;

    ///////////////////////////////////////////////////////////////////////////

    [SerializeField] GameObject[] rightsideButtons;
    [SerializeField] GameObject[] leftsideButtons;


    private const string CAMERA_PERMISSION = "android.permission.CAMERA";


    public void OpenAd()
    {
        Application.OpenURL("market://details?id=" + "com.sks.gpt.chat.ai.mix.animal.marge.master");
    }


    private void OnEnable()
    {
        currentBg.GetComponent<Image>().sprite = bgImages[GetCurrentBgImageIndex()];


        MainMenuScreen.MainMenuAnim -= MainMenuButtonsAnim;
        MainMenuScreen.MainMenuAnim += MainMenuButtonsAnim;
    }
    public void DisableLoadingPanel()
    {
        loadingPanel.SetActive(false);
    }
    public void ToggleObjForScreenshot(bool isActive)
    {
        foreach (GameObject obj in ObjsToDeactiveForScreenshot)
        {
            obj.SetActive(isActive);
        }
    }

    public void ToggleBg()
    {
        if (GetCurrentBgImageIndex() + 1 == bgImages.Length)
        {
            SetCurrentBgImageIndex(0);
        }
        else
        {
            SetCurrentBgImageIndex(GetCurrentBgImageIndex() + 1);
        }
        currentBg.GetComponent<Image>().sprite = bgImages[GetCurrentBgImageIndex()];
    }

   
    private void OnDisable()
    {
        MainMenuScreen.MainMenuAnim -= MainMenuButtonsAnim;
    }
    private void MainMenuButtonsAnim(bool val)
    {
        if (val)
        {
            StartCoroutine(RightSideAnim());
            StartCoroutine(LeftSideAnim());
        }
    }
    IEnumerator RightSideAnim()
    {
        for (int i = 0; i < rightsideButtons.Length; i++)
        {
            rightsideButtons[i].SetActive(true);
            rightsideButtons[i].transform.DOScale(1, 0.2f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.2f);
        }
    }
    IEnumerator LeftSideAnim()
    {
        for (int i = 0; i < leftsideButtons.Length; i++)
        {
            leftsideButtons[i].SetActive(true);
            leftsideButtons[i].transform.DOScale(1, 0.2f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.2f);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
   
    public void ProMode()
    {
        isPro = !isPro;


        if (HasCameraPermission() == true)
        {
            if (isPro)
            {
                arCamera.SetActive(true);
                bg.SetActive(false);

            }
            else
            {
                bg.SetActive(true);
                arCamera.SetActive(false);

                Debug.Log(">>>>>>>>> Simple Mode Activated");
            }
        }
        else
        {
            try
            {
                RequestCameraPermission();
            }
            catch (Exception exc)
            {

            }
            popupPanel.SetActive(true);
        }


    }
    public void PopupDisable()
    {
        popupPanel.SetActive(false);
    }

    ///////////////////////////////////////////////////////////////////////////

    //      Fahad Bhai

    public VapeUnlockPopupHandler VapeUnlockPopupHandler;

    ///////////////////////////////////////////////////////////////////////////
  public  static bool AdShown = false;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(Instance.gameObject);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }





    private bool HasCameraPermission()
    {
#if !UNITY_EDITOR
#if UNITY_ANDROID
        using (var permissionChecker = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (var currentActivity = permissionChecker.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                var permissionStatus = currentActivity.Call<int>("checkSelfPermission", CAMERA_PERMISSION);
                return permissionStatus == 0; // 0 means PERMISSION_GRANTED
            }
        }
#endif
#else
        return true;
#endif
    }

    public void RequestCameraPermission()
    {
#if !UNITY_EDITOR

        if (!HasCameraPermission())
        {
            using (var permissionChecker = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                using (var currentActivity = permissionChecker.GetStatic<AndroidJavaObject>("currentActivity"))
                {
                    string[] permissions = { CAMERA_PERMISSION };
                    currentActivity.Call("requestPermissions", permissions, 0);
                }
            }
        }
#endif

    }





    private void Start()
    {
        for (int i = 0; i < leftsideButtons.Length; i++)
        {
            leftsideButtons[i].SetActive(false);
            leftsideButtons[i].transform.DOScale(0, 0);
        }
        for (int i = 0; i < rightsideButtons.Length; i++)
        {
            rightsideButtons[i].SetActive(false);
            rightsideButtons[i].transform.DOScale(0, 0);
        }




        arCamera.SetActive(false);
        isPro = false;

        Shop.transform.DOScale(0, 0);
        for (int i = 0; i < shopButtons.Length; i++)
        {
            shopButtons[i].SetActive(false);
            shopButtons[i].transform.DOScale(0, 0);
        }
        ItemPurchasePopup.transform.DOScale(0, 0);
        StartCoroutine(MenuCoinAnim());
    }


    IEnumerator MenuCoinAnim()
    {
        coinIcon.transform.DOScale(new Vector3(-1, 1, 1), 2).SetLoops(-1, LoopType.Yoyo);
        yield return new WaitForSeconds(3);
    }


    public static GameManager SharedInstance()
    {
        if (!Instance)
        {
            Instance = FindObjectOfType(typeof(GameManager)) as GameManager;

            if (!Instance)
            {
                var obj = new GameObject("GameManager");
                Instance = obj.AddComponent<GameManager>();
            }
            else
            {
                Instance.gameObject.name = "GameManager";
            }
        }
        return Instance;
    }



    private ItemsShopScriptable allShopAccessories;
    public ItemsShopScriptable AllShopAccessories
    {
        get
        {
            if (!allShopAccessories)
            {
                allShopAccessories = Resources.Load(Constants.ShopItemsAddress) as ItemsShopScriptable;
            }
            return allShopAccessories;
        }
    }

    private SmokeHandler smokeHandler;
    public SmokeHandler SmokeHandler
    {
        get
        {
            if (!smokeHandler)
            {
                smokeHandler = FindObjectOfType<SmokeHandler>();
            }
            return smokeHandler;
        }
    }

    public void StoreCurrentItemInPrefs(string id)
    {
        Item item = AllShopAccessories.GetItemById(id);
        if (item.productType == ProductType.Tank)
        {
            PlayerPrefs.SetString(Constants.CurrentTank, item.id);
        }
        else if (item.productType == ProductType.Battery)
        {
            PlayerPrefs.SetString(Constants.CurrentBattery, item.id);
        }
    }

    public void SetCurrentBgImageIndex(int index)
    {
        PlayerPrefs.SetInt(Constants.CurrentBgImageIndex, index);
    }
    public int GetCurrentBgImageIndex()
    {
        return PlayerPrefs.GetInt(Constants.CurrentBgImageIndex, 0);
    }
    public void GetCoinsForRewardedVideo()
    {

        coinsCounter.AddCoins(GameManager.SharedInstance().watchVideoCoinsToAward);
        WatchAVideo.SetActive(true);
    }

    public void EquipItem(ShopItem purchased_item)
    {
        switch (purchased_item.productType)
        {
            case ProductType.Tank:
                current_vape.VapeTank.sprite = purchased_item.productImage.sprite;
                Shop.SetActive(false);
                break;
            case ProductType.Battery:
                current_vape.VapeBattery.sprite = purchased_item.productImage.sprite;
                Shop.SetActive(false);
                break;
            case ProductType.Juice:
                RefillFluid();
                Shop.SetActive(false);
                break;
        }
    }
    public void EnableSmokeParent()
    {
        smokeParticleParent.SetActive(true);
        smoke.gameObject.SetActive(true);
    }
    public void Refill()
    {
        StartCoroutine(RefillVape());
    }

    IEnumerator RefillVape()
    {
        while (current_vape.Fluid.fillAmount < 1)
        {
            current_vape.Fluid.fillAmount += 0.5f * Time.deltaTime;
            yield return null;
        }
        current_vape.Fluid.fillAmount = 1;
        PlayerPrefs.SetFloat("Juice", current_vape.Fluid.fillAmount);
        current_vape.SetVapeAccessories();
    }
    public void DisableSmokeParent()
    {
        smokeParticleParent.SetActive(false);
        smoke.gameObject.SetActive(false);
    }
    public void OpenShop()
    {
        DisableSmokeParent();
        Shop.SetActive(true);
        Shop.transform.DOScale(1, 0);

        StartCoroutine(ShopAnim());

        AudioManager.SharedInstance().Play_Sound(Constants.ClickSound);
        if (smoke.isPlaying)
            smoke.Stop();
    }

    IEnumerator ShopAnim()
    {
        for (int i = 0; i < shopButtons.Length; i++)
        {
            shopButtons[i].SetActive(true);
            shopButtons[i].transform.DOScale(1, 0.2f);

            yield return new WaitForSeconds(0.1f);
        }
    }
    public void EnableSmoke(string checkState)
    {
        GameManager.Instance.SmokeHandler.isAlive = true;
        GameManager.SharedInstance().EnableSmokeParent();
    }

    public void CloseShop()
    {
        //if (AdsManager.Instance && AdsManager.Instance.IsInterstitialLoaded)
        //    AdsManager.Instance.ShowInterstitialAd();
        AudioManager.SharedInstance().Play_Sound(Constants.ClickSound);
        for (int i = 0; i < shopButtons.Length; i++)
        {
            shopButtons[i].transform.DOScale(0, 0);
        }
        HideWatchAVideoPopup();
        Shop.SetActive(false);
        ItemPurchasePopup.SetActive(false);
    }

    public void ShowWatchAVideoPopup()
    {
        watchVideoCoinsToAward = 50;
        WatchAVideo.SetActive(true);
        AudioManager.SharedInstance().Play_Sound(Constants.ClickSound);
    }

    public void HideWatchAVideoPopup()
    {
        WatchAVideo.SetActive(false);
    }

    public void OnClickRefill()
    {
        AudioManager.SharedInstance().Play_Sound(Constants.ClickSound);
        ShopItem fluid = new ShopItem();
        fluid.id = "juc_def";
        fluid.productType = ProductType.Juice;
        currently_purchased_item = fluid;
    }

    public void RefillFluid()
    {
        RefillPanel.SetActive(false);
    }

    private ButtonInhale buttonInhale;
    public ButtonInhale ButtonInhale
    {
        get
        {
            if (!buttonInhale)
            {
                buttonInhale = FindObjectOfType<ButtonInhale>();
            }
            return buttonInhale;
        }
    }

    private ChoseSmokeType choseSmoleType;
    public ChoseSmokeType ChoseSmokeType
    {
        get
        {
            if (!choseSmoleType)
            {
                choseSmoleType = FindObjectOfType<ChoseSmokeType>();
            }
            return choseSmoleType;
        }
    }

    private CoinsCounter coinCounter;
    public CoinsCounter CoinCounter
    {
        get
        {
            if (!coinCounter)
            {
                coinCounter = FindObjectOfType<CoinsCounter>();
            }
            return coinCounter;
        }
    }

    private SettingMain settingMain;
    public SettingMain SettingMain
    {
        get
        {
            if (!settingMain)
            {
                settingMain = FindObjectOfType<SettingMain>();
            }
            return settingMain;
        }
    }

    private PurchaseRingsPopup purchaseRingsPopup;
    public PurchaseRingsPopup PurchaseRingsPopup
    {
        get
        {
            if (!purchaseRingsPopup)
            {
                purchaseRingsPopup = FindObjectOfType<PurchaseRingsPopup>();
            }
            return purchaseRingsPopup;
        }
    }

    private ButtonStateManager buttonStateManager;
    public ButtonStateManager ButtonStateManager
    {
        get
        {
            if (!buttonStateManager)
            {
                buttonStateManager = FindObjectOfType<ButtonStateManager>();
            }
            return buttonStateManager;
        }
    }

    private SliderArrowState sliderArrowState;
    public SliderArrowState SliderArrowState
    {
        get
        {
            if (!sliderArrowState)
            {
                sliderArrowState = FindObjectOfType<SliderArrowState>();
            }
            return sliderArrowState;
        }
    }

    private TextScale textScale;
    public TextScale TextScale
    {
        get
        {
            if (!textScale)
            {
                textScale = FindObjectOfType<TextScale>();
            }
            return textScale;
        }
    }
    public static string RingsEnergyFillImageValueKey = "RingsEnergyFillImageValue";
}
