using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class MainMenuScreen : MonoBehaviour
{
    [SerializeField] Button Continue;
    [SerializeField] Text TapToContinue;
    [SerializeField] GameObject bg;
    public static System.Action<bool> MainMenuAnim;

    void Start()
    {
          MainMenuAnim?.Invoke(true);
            gameObject.SetActive(false);
            bg.SetActive(false);
       
    }

}
