using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TextScale : MonoBehaviour
{
    private Vector3 orignalScale;
    private Vector3 scaleTo;

    private void Start()
    {
        //ScaleText();
    }

    public void ScaleText()
    {
        this.GetComponent<RectTransform>().transform.DOScaleY(1.5f, 0.5f).SetEase(Ease.InOutSine).SetLoops(6, LoopType.Yoyo);
    }
}
