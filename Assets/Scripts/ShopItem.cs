using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public enum ProductType { Tank, Battery, Juice}
public class ShopItem : MonoBehaviour
{
    public string id;
    public Image productImage;
    public ProductType productType;
    [SerializeField] Button equipButton;


    void Start()
    {
       
        equipButton.onClick.AddListener(() =>
        {
            GameManager.Instance.EnableSmokeParent();
            transform.DOScale(1.01f, 0.1f).SetEase(Ease.Linear).OnComplete(() =>
            {
                transform.DOScale(1, 0.1f);
            });
            
            GameManager.SharedInstance().currently_purchased_item = null;
            GameManager.SharedInstance().currently_purchased_item = this;
            GameManager.SharedInstance().Shop.GetComponent<Shop>().Selected_item = this;
            AudioManager.SharedInstance().Play_Sound(Constants.ClickSound);
            GameManager.Instance.EquipItem(this);
        });


    }


    void PurchaseItem()
    {
        
    }

    void ShowHideButtons()
    {
        if (PlayerPrefs.HasKey(id))
        {
            equipButton.enabled = true;
        }
        else
        {
            equipButton.enabled = false;
        }
    }

}
