using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{

    static AudioManager Instance { get; set; }
    public Sound[] all_sounds;
    public AudioSource BackgroundMusic1;
    public Sound RecentSoundEffect;
    public Sound InhaleSound;
    public Sound ExhaleSound;



    private void Awake()
    {
        foreach (Sound sound in all_sounds)
        {
            sound.audio_source = gameObject.AddComponent<AudioSource>();
            sound.audio_source.clip = sound.clip;
            sound.audio_source.volume = sound.volume;
        }

        BackgroundMusic1 = Play_Sound(Constants.BgMusic);
        BackgroundMusic1.loop = true;

        InhaleSound = Array.Find(all_sounds, sound => sound.sound_name == Constants.InhaleSound);
        ExhaleSound = Array.Find(all_sounds, sound => sound.sound_name == Constants.ExhaleSound);
    }

    public static AudioManager SharedInstance()
    {
        if (!Instance)
        {
            Instance = FindObjectOfType(typeof(AudioManager)) as AudioManager;

            if (!Instance)
            {
                var obj = new GameObject("AudioManager");
                Instance = obj.AddComponent<AudioManager>();
            }
            else
            {
                Instance.gameObject.name = "AudioManager";
            }
        }
        return Instance;
    }

    public void Enable_All_Sounds()
    {
        foreach (Sound sound in all_sounds)
        {
            Destroy(sound.audio_source);
        }
    }

    public void Disable_All_Sounds()
    {
        foreach (Sound sound in all_sounds)
        {
            sound.audio_source = gameObject.AddComponent<AudioSource>();
        }
    }

    public AudioSource Play_Sound(string sound_name)
    {
        var sound = Array.Find(all_sounds, sound => sound.sound_name == sound_name);
        if (!sound.audio_source.isPlaying)
        {
            sound.audio_source.Play();
        }
        return sound.audio_source;
    }

    public AudioSource PlayInhaleSound()
    {
        if (ExhaleSound.audio_source.isPlaying)
        {
            ExhaleSound.audio_source.Stop();
        }
        InhaleSound.audio_source.Play();
        RecentSoundEffect = InhaleSound;
        return InhaleSound.audio_source;
    }


    public AudioSource PlayExhaleSound()
    {
        if (RecentSoundEffect == InhaleSound)
        {

            if (InhaleSound.audio_source.isPlaying)
            {
                InhaleSound.audio_source.Stop();
            }
            ExhaleSound.audio_source.Play();
            RecentSoundEffect = ExhaleSound;

        }
        return ExhaleSound.audio_source;
    }

    [System.Serializable]
    public class Sound
    {

        public string sound_name;
        public AudioClip clip;

        [Range(0, 1)]
        public float volume;

        [HideInInspector]
        public AudioSource audio_source;
    }

}

