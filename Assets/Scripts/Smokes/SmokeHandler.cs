using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//      ::::: Enum :::::

public enum SmokeTypes
{
    Simple,
    Rings,
    Squares,
    Donuts,
    Rectangles
}

public class SmokeHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    ////////////////////////////////////////////////////////////////////////////


    private int smokeNumber;

    int smokeIndex;

    int countTouch;
    public SmokeTypes CurrentSmokeType;
    private SmokeTypes inType;

    bool flash;
    bool val;

    [SerializeField] GameObject blinkingimage;

    public static System.Action OnSmokeStart;
    //          ::::: Main variables & Objects ::::

    public Image InhaleBtnImage;
    [SerializeField] Sprite onImage;
    [SerializeField] Sprite offImage;
    [SerializeField] Sprite BlueBarImage;
    [SerializeField] Sprite RedBarImage;
    [SerializeField] Image FillImage;
    [SerializeField] public Image RingsEnergyFillImage;
    private ParticleSystem smoke, grow;
    private float smokeDuration;
    private Inhaler Inhaler;
    private bool CanExhaleSmoke;
    private AudioSource InhaleAS;
    private AudioSource ExhaleAS;
    public bool isPressed, isAlive;

    public Button vibrateButton;
    public Sprite vibrateOnImage, vibrateOffImage;
    bool isVibrate;

    //          ::::: Smoke Rings :::::

    [SerializeField] List<ParticleSystem> SmokeRings;
    public System.Action OnSmokeEnergyBarEmpty;

    //          ::::: Sphares Rings :::::

    [SerializeField] List<ParticleSystem> SphareRings;

    //          ::::: Donuts Rings :::::

    [SerializeField] List<ParticleSystem> DonutsRings;

    //          ::::: Rectangles Rings :::::

    [SerializeField] List<ParticleSystem> RectanglesRings;


    float JuicePref;
    //          ::::: Methods :::::
    private void Awake()
    {
        JuicePref = PlayerPrefs.GetFloat("Juice", 1);
        FillImage.fillAmount = JuicePref;
        countTouch = 0;
    }


    void Start()
    {
        isVibrate = true;
        val = true;
        CurrentSmokeType = SmokeTypes.Simple;

        CanExhaleSmoke = true;
        Inhaler = GameManager.SharedInstance().Inhaler;
        Inhaler.InhaleProgressImage.fillAmount = 0;
        smoke = GameManager.SharedInstance().smoke;
        grow = GameManager.SharedInstance().grow;

        //  Rings

        OnSmokeEnergyBarEmpty -= OnSmokeBarEmpty;
        OnSmokeEnergyBarEmpty += OnSmokeBarEmpty;

        isAlive = true;

        //      Hide Health Slider

        GameManager.SharedInstance().HealthSlider.SetActive(false);

    }//     Start

    void OnSmokeBarEmpty()      //     Rings
    {
        GameManager.Instance.SmokeEnergyRefillPanel.SetActive(true);
    }


    public void OnPointerUp(PointerEventData eventData)     //     Up Pointer
    {
        smokeDuration = Inhaler.InhaleProgressImage.fillAmount * 500;
        if (smokeDuration < 0.10f)
        {
            smokeDuration = Inhaler.InhaleProgressImage.fillAmount * 500;
        }
        isPressed = false;
        InhaleBtnImage.sprite = offImage;
        StartCoroutine(UnfillSlider());
        InhaleAS.Stop();
        if (CanExhaleSmoke)
        {
            ExhaleAS = AudioManager.SharedInstance().PlayExhaleSound();
            GameManager.SharedInstance().coinsCounter.AddCoins(Convert.ToInt32(smokeDuration / 10));
        }

        //if (IGFlashlight.HasTorch)
        //{
        //    IGFlashlight.EnableFlashlight(false);
        //}
    }


    public void OnPointerDown(PointerEventData eventData)       //     Pointer Down
    {

        RingsEnergyFillImage.fillAmount = PlayerPrefs.GetFloat(GameManager.RingsEnergyFillImageValueKey, 1);

        if (GameManager.SharedInstance().current_vape.Fluid.fillAmount > 0.1f)
        {
            if ((RingsEnergyFillImage.fillAmount > 0 && CurrentSmokeType != SmokeTypes.Simple) || CurrentSmokeType == SmokeTypes.Simple)
            {
                if (!(smoke.isPlaying))
                {
                    if (FillImage.fillAmount > 0.1f)
                    {
                        Debug.Log("==========> isAlive " + GameManager.Instance.SmokeHandler.isAlive);
                        if (isAlive)
                        {
                            isPressed = true;
                        }
                        InhaleBtnImage.sprite = onImage;
                        StartCoroutine(ConsumeJuice());
                        StartCoroutine(FillSlider());
                        InhaleAS = AudioManager.SharedInstance().PlayInhaleSound();
                    }
                    else
                    {
                        InhaleBtnImage.sprite = offImage;
                    }
                }
            }
            else
            {
                GameManager.SharedInstance().SmokeEnergyRefillPanel.SetActive(true);
            }
        }
        else
        {
            GameManager.SharedInstance().RefillPanel.SetActive(true);
        }
        //if (IGFlashlight.HasTorch)
        //{
        //    IGFlashlight.EnableFlashlight(true);
        //}
    }


    IEnumerator ConsumeJuice()      //     Juice Consimption
    {
        if (CurrentSmokeType == SmokeTypes.Simple)
        {
            while (isPressed && FillImage.fillAmount > 0)
            {

                PlayerPrefs.SetFloat("Juice", FillImage.fillAmount);                /////
                yield return new WaitForEndOfFrame();
                FillImage.fillAmount -= Time.deltaTime * (-0.16f);
                if (FillImage.fillAmount <= 0)
                {
                    GameManager.SharedInstance().RefillPanel.SetActive(true);
                    isAlive = false;
                }
                else
                {
                    isAlive = true;
                }
            }
        }
        else
        {
            while (isPressed && FillImage.fillAmount > 0)
            {
                PlayerPrefs.SetFloat("Juice", FillImage.fillAmount);                /////
                yield return new WaitForEndOfFrame();
                FillImage.fillAmount -= Time.deltaTime * (-0.13f);
                if (FillImage.fillAmount <= 0)
                {
                    GameManager.SharedInstance().RefillPanel.SetActive(true);
                    isAlive = false;
                }
                else
                {
                    isAlive = true;
                }
            }
        }
    }

    private void Update()
    {
        if (isPressed && isVibrate)
        {
            Handheld.Vibrate();
        }
    }

    public void Vibratation()
    {
        isVibrate = !isVibrate;
        if (isVibrate)
        {
            vibrateButton.GetComponent<Image>().sprite = vibrateOnImage;
        }
        else
        {
            vibrateButton.GetComponent<Image>().sprite = vibrateOffImage;
        }
    }
    IEnumerator FillSlider()        //     Fill Bar
    {
        while (isPressed)
        {
            yield return new WaitForEndOfFrame();
            Inhaler.InhaleProgressImage.fillAmount += (Time.deltaTime * 50) / 100;
            if (RingsEnergyFillImage.fillAmount > 0)
            {

                if (CurrentSmokeType == SmokeTypes.Rings)
                {
                    RingsEnergyFillImage.fillAmount -= Time.deltaTime / 15;
                }
                if (CurrentSmokeType == SmokeTypes.Squares)
                {
                    RingsEnergyFillImage.fillAmount -= Time.deltaTime / 15;
                }
                if (CurrentSmokeType == SmokeTypes.Donuts)
                {
                    RingsEnergyFillImage.fillAmount -= Time.deltaTime / 15;
                }
                if (CurrentSmokeType == SmokeTypes.Rectangles)
                {
                    RingsEnergyFillImage.fillAmount -= Time.deltaTime / 15;
                }
            }
            if (FillImage.fillAmount > 0)
            {
                FillImage.fillAmount -= Time.deltaTime / 4.5f;
            }
            else
            {
                OnSmokeEnergyBarEmpty?.Invoke();
            }
            if (Inhaler.InhaleProgressImage.fillAmount >= 0.8)
            {
                Inhaler.InhaleProgressImage.sprite = RedBarImage;
            }
            if (Inhaler.InhaleProgressImage.fillAmount >= 1)
            {
                Inhaler.InhaleProgressImage.sprite = RedBarImage;
                StartCoroutine(ExecuteDelay());
            }
        }
        PlayerPrefs.SetFloat(GameManager.RingsEnergyFillImageValueKey, RingsEnergyFillImage.fillAmount);
    }

    
    IEnumerator ExecuteDelay()      //     Delay -> Still Pressed When Bar Is Filler Up
    {

        yield return new WaitForSeconds(0.3f);
        if (isPressed)
        {
            isPressed = false;
            StartCoroutine(UnfillSliderWithoutSmoke());
        }

    }


    IEnumerator UnfillSliderWithoutSmoke()      //     Unfill ->   Redbar Without Smoke
    {
        CanExhaleSmoke = false;
        AudioSource aSource = AudioManager.SharedInstance().Play_Sound(Constants.CoughSound);
        while (!(isPressed) && Inhaler.InhaleProgressImage.fillAmount > 0 && !CanExhaleSmoke)
        {
            yield return new WaitForEndOfFrame();
            Inhaler.InhaleProgressImage.fillAmount -= (Time.deltaTime * 50) / 100;
            Inhaler.InhaleProgressImage.sprite = RedBarImage;
            if (Inhaler.InhaleProgressImage.fillAmount <= 0)
            {
                CanExhaleSmoke = true;
                Inhaler.InhaleProgressImage.sprite = BlueBarImage;
            }
            isAlive = false;
        }
        isAlive = true;
        RateUS.Instance.ShowPopUp();
    }


    IEnumerator UnfillSlider()      //     Unfill Slider
    {
        float TimeElapsed = 0;
        //smokeIndex = UnityEngine.Random.Range(0, GameManager.Instance.smokesColors.Length);
        while (!(isPressed) && Inhaler.InhaleProgressImage.fillAmount > 0 && CanExhaleSmoke)
        {
            TimeElapsed += Time.deltaTime;
            yield return new WaitForEndOfFrame();
            Inhaler.InhaleProgressImage.fillAmount -= (Time.deltaTime * 50) / 100;
            float WaitTime = UnityEngine.Random.Range(0.6f, 1f);
            PlaySmoke(smokeDuration, CurrentSmokeType);
            if (Inhaler.InhaleProgressImage.fillAmount <= 0.03)
            {
                smoke.Stop();
                grow.Stop();
            }
            if (TimeElapsed > WaitTime)
            {
                PlaySmoke(smokeDuration, CurrentSmokeType);
                TimeElapsed = 0;
            }
        }
        Inhaler.InhaleProgressImage.sprite = BlueBarImage;

    }


    private void OnEnable()
    {
        SettingMain.SmokeIndex -= SetSmokeIndex;
        SettingMain.SmokeIndex += SetSmokeIndex;
    }
    private void OnDisable()
    {
        SettingMain.SmokeIndex -= SetSmokeIndex;
    }
    private void SetSmokeIndex(int val)
    {
        smokeNumber = val;
    }



    void PlaySmoke(float duration, SmokeTypes type)     //     Play Smoke According To The Selected Condition
    {
        inType = type;
        if (!(smoke.isPlaying))
        {
            OnSmokeStart?.Invoke();
        }
        switch (type)
        {
            ////////////////////////////////////////////////////////////////////                Simple

            case SmokeTypes.Simple:
                if (!(smoke.isPlaying))
                {
                    var _main = smoke.main;
                    smoke.emissionRate = duration;
                    _main.duration = duration / 20f;
                    smoke.Play();
                    StartCoroutine(FlashOnOff(smoke));
                    StartCoroutine(ImageOnOff(smoke));
                }
                break;

            ////////////////////////////////////////////////////////////////////                Rings

            case SmokeTypes.Rings:
                ParticleSystem particle = null;
                SmokeRings.ForEach((p) =>
                {
                    if (!p.isPlaying && !particle)
                    {
                        particle = p;
                        particle.startColor = Color.white;
                    }

                });
                if (particle && GameManager.SharedInstance().current_vape.Fluid.fillAmount >= 0)
                {

                    particle.Play();
                    StartCoroutine(FlashOnOff(particle));
                    StartCoroutine(ImageOnOff(particle));
                    particle = null;
                }
                break;

            ////////////////////////////////////////////////////////////////////                Sphare

            case SmokeTypes.Squares:
                ParticleSystem SphareParticle = null;
                SphareRings.ForEach((p) =>
                {
                    if (!p.isPlaying && !SphareParticle)
                    {
                        SphareParticle = p;
                        SphareParticle.startColor = Color.white;
                    }

                });

                if (SphareParticle && GameManager.SharedInstance().current_vape.Fluid.fillAmount >= 0)
                {
                    SphareParticle.Play();
                    StartCoroutine(FlashOnOff(SphareParticle));
                    StartCoroutine(ImageOnOff(SphareParticle));
                    SphareParticle = null;
                }
                break;

            ////////////////////////////////////////////////////////////////////                Donuts

            case SmokeTypes.Donuts:
                ParticleSystem DonutsParticle = null;
                DonutsRings.ForEach((p) =>
                {
                    if (!p.isPlaying && !DonutsParticle)
                    {
                        DonutsParticle = p;
                        DonutsParticle.startColor = Color.white;
                    }

                });
                if (DonutsParticle && GameManager.SharedInstance().current_vape.Fluid.fillAmount >= 0)
                {
                    DonutsParticle.Play();
                    StartCoroutine(FlashOnOff(DonutsParticle));
                    StartCoroutine(ImageOnOff(DonutsParticle));
                    DonutsParticle = null;
                }
                break;

            ////////////////////////////////////////////////////////////////////                Rectangles

            case SmokeTypes.Rectangles:
                ParticleSystem RectangleParticle = null;
                RectanglesRings.ForEach((p) =>
                {
                    if (!p.isPlaying && !RectangleParticle)
                    {
                        RectangleParticle = p;
                        RectangleParticle.startColor = Color.white;
                    }

                });
                if (RectangleParticle && GameManager.SharedInstance().current_vape.Fluid.fillAmount >= 0)
                {
                    RectangleParticle.Play();
                    StartCoroutine(FlashOnOff(RectangleParticle));
                    StartCoroutine(ImageOnOff(RectangleParticle));
                    RectangleParticle = null;
                }
                break;

            ////////////////////////////////////////////////////////////////////                Default

            default:
                break;
        }
        
    }


    IEnumerator FlashOnOff(ParticleSystem p)
    {

        while (p.IsAlive())
        {
            Debug.Log(">>>>>>>>> Working");
#if UNITY_IOS
            if (IGFlashlight.HasTorch)
            {
                IGFlashlight.EnableFlashlight(true);
            }
             yield return Yielders.Get(0.1f);
            if (IGFlashlight.HasTorch)
            {
                IGFlashlight.EnableFlashlight(false);
            }
#else
            yield return Yielders.Get(0f);
#endif
        }

    }
    IEnumerator ImageOnOff(ParticleSystem p)
    {
        //while (p.IsAlive())
        //{
        //    blinkingimage.SetActive(true);
        //    yield return Yielders.Get(0.05f);
        //    blinkingimage.SetActive(false);
        //    yield return Yielders.Get(0.05f);
        //}
        yield return Yielders.Get(0f);
    }



    public void ResetFillImage()
    {
        FillImage.fillAmount = 1;
    }
}//     Class