using UnityEngine;
using UnityEngine.UI;

public enum ButtonType
{
    Simple,
    Rings,
    Squares,
    Donuts,
    Rectangles
}

public class SettingMain : MonoBehaviour
{
    public static System.Action<int> SmokeIndex;
    public static System.Action<SmokeTypes> OnSmokePurchase;
    private int randomNumber;
    [HideInInspector] public Image img;
    [HideInInspector] public Transform trans;

    public ButtonType type;
    Button pressHandler;

    public Text coins;

    //      Locked State

    public bool LockState;
    public bool ChangeLock;

    public bool A = false;
    private Inhaler inhaler;
    void Awake()
    {
        OnSmokePurchase += PurchaseSmoke;
        pressHandler = GetComponent<Button>();
        inhaler = GameManager.SharedInstance().HealthSlider.GetComponent<Inhaler>();
        img = GetComponent<Image>();
        trans = transform;

        LockState = true;
        ChangeLock = true;
    }
    private void OnDestroy()
    {
        OnSmokePurchase -= PurchaseSmoke;
    }
    private void Start()
    {
        pressHandler.onClick.AddListener(() =>
        {
            SendState();
        });
    }
    public void SetSimpleState()
    {
        type = ButtonType.Simple;
    }
   public void SendState()
    {
        if (inhaler == null)
        {
            inhaler= GameManager.SharedInstance().HealthSlider.GetComponent<Inhaler>();
        }
        SmokeIndex?.Invoke(randomNumber);
     
        if (type == ButtonType.Simple)
        {
            GameManager.SharedInstance().HealthSlider.SetActive(false);
            GameManager.Instance.ButtonStateManager.ManageStateOfButtons("Simple");
            GameManager.Instance.SmokeHandler.CurrentSmokeType = SmokeTypes.Simple;
        }
        else
        {
            if (!PlayerPrefs.HasKey(type.ToString()))
            {
                GameManager.Instance.SmokeEnergyRefillPanel.SetActive(true);
                GameManager.Instance.PurchaseRingsPopup.Title.text = "Refill Smoke "+ type+ " Energy";
                GameManager.Instance.PurchaseRingsPopup.refillImage.sprite = GameManager.Instance.TypeIconsStates[(int)type].ActiveState;
                GameManager.Instance.PurchaseRingsPopup.checkState = (ButtonStates)type;
                if (GameManager.Instance.PurchaseRingsPopup.ring == false)
                {
                    //HandleUnlock();
                }
            }
            else
            {
               
                EnableSmoke(type.ToString());
            }
        }
    }
    void PurchaseSmoke(SmokeTypes _type)
    {
        GameManager.Instance.SmokeHandler.CurrentSmokeType = _type;
    }
    void HandleUnlock()
    {
        GameManager.Instance.SmokeEnergyRefillPanel.SetActive(true);

        GameManager.Instance.SmokeHandler.isAlive = false;
        Debug.Log("==========> isAlive " + GameManager.Instance.SmokeHandler.isAlive);
    }public void EnableSmoke(string checkState)
    {
        GameManager.SharedInstance().HealthSlider.SetActive(true);
        inhaler.InhaleProgressImage.fillAmount = PlayerPrefs.GetFloat(GameManager.RingsEnergyFillImageValueKey, 1);
        GameManager.Instance.SmokeHandler.isAlive = true;
        GameManager.Instance.ButtonStateManager.ManageStateOfButtons(checkState);
        GameManager.SharedInstance().EnableSmokeParent();

    }
}




//GameManager.Instance.SmokeHandler.ResetFillImage();