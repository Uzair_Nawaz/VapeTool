using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SettingItems : MonoBehaviour
{
    [Header("space between menu items")]
    [SerializeField] Vector2 spacing;
    [SerializeField] Button mainButton;
    SettingMain[] menuItems;
    bool isExpanded = false;
    Vector2 mainButtonPosition;
    int itemCount;

    [Space]
    [Header("Main button rotation")]
    [SerializeField] float rotationDuration;
    [SerializeField] Ease rotationEase;

    [Space]
    [Header("Animation")]
    [SerializeField] float expendDuration;
    [SerializeField] float collapseDuration;
    [SerializeField] Ease expandEasy;
    [SerializeField] Ease collapseEasy;

    [Space]
    [Header("Fading")]
    [SerializeField] float expandFadeDuration;
    [SerializeField] float collapseFadeDuration;

    void Start()
    {
        itemCount = transform.childCount - 1;
        menuItems =GetComponentsInChildren<SettingMain>();
        //                                                                          for (int i = 0; i < itemCount; i++)
        //                                                                          {
        //                                                                              menuItems[i] = transform.GetChild(i + 1).GetComponent<SettingMain>();
        //                                                                              menuItems[i].transform.position = Vector3.zero;
        //                                                                          }
        //                                                                          mainButton = transform.GetChild(0).GetComponent<Button>();
        mainButton.onClick.AddListener(ToggleMenu);
        mainButton.transform.SetAsLastSibling();
        mainButtonPosition = mainButton.transform.GetComponent<RectTransform>().anchoredPosition;
        ResetPosition();
    }

    void ResetPosition()
    {
        for (int i = 0; i < itemCount; i++)
        {
            menuItems[i].transform.GetComponent<RectTransform>().anchoredPosition = mainButtonPosition;
        }
    }

    void ToggleMenu()
    {
        isExpanded = !isExpanded;
        if (isExpanded)
        {
            for (int i = 0; i < itemCount; i++)
            {
                //                                                                  menuItems [i].trans.position = mainButtonPosition + spacing * (i + 1);
                //                                                                  Vector2 targetPosition = mainButtonPosition + spacing * (i + 1);

                menuItems[i].trans.DOMoveY((i-5) * spacing.y-35, expendDuration).SetEase(expandEasy);   //(i*spacing.y)
                menuItems[i].img.DOFade(1f, expandFadeDuration).From(0f);
            }
            GameManager.Instance.SliderArrowState.ActiveImage.sprite = GameManager.Instance.SliderArrowState.UpArrow;
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                //menuItems[i].trans.position = mainButtonPosition;
                menuItems[i].trans.DOLocalMoveY(0, collapseDuration).SetEase(collapseEasy);
                menuItems[i].img.DOFade(0f, collapseDuration);
            }
            GameManager.Instance.SliderArrowState.ActiveImage.sprite = GameManager.Instance.SliderArrowState.DownArrow;
        }
        //      Main Button Rotation
        mainButton.transform.DORotate(Vector3.forward * 360f, rotationDuration).From(Vector3.zero).SetEase(rotationEase);
    }

    void OnDestroy()
    {
        mainButton.onClick.RemoveListener(ToggleMenu);
    }
}
