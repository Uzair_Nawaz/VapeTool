using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonInhale : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Image InhaleBtnImage;
    [SerializeField] Sprite onImage;
    [SerializeField] Sprite offImage;
    [SerializeField] Sprite BlueBarImage;
    [SerializeField] Sprite RedBarImage;
    [SerializeField] Image FillImage;
    private ParticleSystem smoke, grow;
    private float smokeDuration;
    //private Slider slider;
    private Inhaler Inhaler;
    private bool CanExhaleSmoke;
    private AudioSource InhaleAS;
    private AudioSource ExhaleAS;
    bool isPressed;



    void Start()
    {
        CanExhaleSmoke = true;
        //slider = GameManager.SharedInstance().slider;
        Inhaler = GameManager.SharedInstance().Inhaler;
        Inhaler.InhaleProgressImage.fillAmount = 0;
        smoke = GameManager.SharedInstance().smoke;
        grow = GameManager.SharedInstance().grow;
    }

    void Update()
    {
        
    }

    

    public void OnPointerUp(PointerEventData eventData)
    {
        smokeDuration = Inhaler.InhaleProgressImage.fillAmount * 100;
        isPressed = false;
        InhaleBtnImage.sprite = offImage;
        StartCoroutine(UnfillSlider());
        InhaleAS.Stop();
        if (CanExhaleSmoke)
        {
            ExhaleAS = AudioManager.SharedInstance().PlayExhaleSound();
            GameManager.SharedInstance().coinsCounter.AddCoins(Convert.ToInt32(smokeDuration / 10));
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("<<<<<<<<< ButtonInhale");
        if (GameManager.SharedInstance().current_vape.Fluid.fillAmount > 0)
        {
            if (!(smoke.isPlaying))
            {  
                if (FillImage.fillAmount > 0)
                {
                    isPressed = true;
                    InhaleBtnImage.sprite = onImage;
                    StartCoroutine(ConsumeJuice());
                    StartCoroutine(FillSlider());
                    InhaleAS = AudioManager.SharedInstance().PlayInhaleSound();
                }
                else
                {
                    InhaleBtnImage.sprite = offImage;
                }
            }
        }
        else
        {
            GameManager.SharedInstance().RefillPanel.SetActive(true);
        }
    }

    IEnumerator ConsumeJuice()
    {
        while (isPressed && FillImage.fillAmount > 0)
        {
            yield return new WaitForEndOfFrame();
            FillImage.fillAmount -= Time.deltaTime * 0.05f;
            if (FillImage.fillAmount <= 0)
                GameManager.SharedInstance().RefillPanel.SetActive(true);
        }

    }

    IEnumerator FillSlider()
    {
        while (isPressed)
        {
            yield return new WaitForEndOfFrame();
            //slider.value += Time.deltaTime * 50;
            Inhaler.InhaleProgressImage.fillAmount += (Time.deltaTime * 50) / 100;
            if(Inhaler.InhaleProgressImage.fillAmount >= 1)
            {
                StartCoroutine(ExecuteDelay());
            }
        }
    }

    IEnumerator ExecuteDelay()
    {
        yield return new WaitForSeconds(0.3f);
        if (isPressed)
        {
            isPressed = false;
            StartCoroutine(UnfillSliderWithoutSmoke());
        }
            
    }

    IEnumerator UnfillSliderWithoutSmoke()
    {
        CanExhaleSmoke = false;
        AudioSource aSource = AudioManager.SharedInstance().Play_Sound(Constants.CoughSound);
        while (!(isPressed) && Inhaler.InhaleProgressImage.fillAmount > 0 && !CanExhaleSmoke)
        {
            yield return new WaitForEndOfFrame();
            Inhaler.InhaleProgressImage.fillAmount -= (Time.deltaTime * 50) / 100;
            Inhaler.InhaleProgressImage.sprite = RedBarImage;
            if (Inhaler.InhaleProgressImage.fillAmount <= 0)
            {
                CanExhaleSmoke = true;
                Inhaler.InhaleProgressImage.sprite = BlueBarImage;
            }
                
        }
    }

    IEnumerator UnfillSlider()
    {
        while (!(isPressed) && Inhaler.InhaleProgressImage.fillAmount > 0 && CanExhaleSmoke)
        {
            yield return new WaitForEndOfFrame();
            Inhaler.InhaleProgressImage.fillAmount -= (Time.deltaTime * 50) / 100;
            PlaySmoke(smokeDuration);
            if (Inhaler.InhaleProgressImage.fillAmount <= 0.3)
            {
                smoke.Stop();
                grow.Stop();
            }

        }
    }

    void PlaySmoke(float duration)
    {
        if (!smoke.isPlaying && !grow.isPlaying)
        {
            var _main = smoke.main;
            var _growMain = grow.main;

            smoke.emissionRate = duration;
            grow.emissionRate = duration;

            _main.duration = duration / 20f;
            _growMain.duration = duration / 20f;

            grow.Play();
            smoke.Play();
           
        }
    }
}
