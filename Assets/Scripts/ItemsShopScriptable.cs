using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ShopItems")]
public class ItemsShopScriptable : ScriptableObject
{
    public List<Item> items;

    public Item GetItemById(string id)
    {
        Item item = new Item();
        items.ForEach((i)=>
        {
            if(i.id == id)
            {
                item.id = i.id;
                item.productImage = i.productImage;
                item.productType = i.productType;
            }
        });
        return item;
    }
    
}
[System.Serializable]
public class Item
{
    public string id;
    public Sprite productImage;
    public ProductType productType;
}
