using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Disclamier : MonoBehaviour
{
    [SerializeField] GameObject understandBtn;
    private void Start()
    {
        understandBtn.AddComponent<Button>().onClick.AddListener(() =>
        {
            DisableDisclamier();
        });

        if (!PlayerPrefs.HasKey("Disclamier"))
        {
            PlayerPrefs.SetInt("Disclamier", 1);
        }
        else
        {
            DisableDisclamier();
        }
    }

    private void DisableDisclamier()
    {
        gameObject.SetActive(false);
    }
}
