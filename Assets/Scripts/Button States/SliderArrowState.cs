using UnityEngine;
using UnityEngine.UI;

public class SliderArrowState : MonoBehaviour
{
    [SerializeField] public Image ActiveImage;
    [SerializeField] public Sprite UpArrow;
    [SerializeField] public Sprite DownArrow;
}
