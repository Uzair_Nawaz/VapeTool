using System;
using UnityEngine;
using UnityEngine.UI;

public class ButtonStateManager : MonoBehaviour
{
    //Generic method
    private void Start()
    {
        for (int i = 0; i < GameManager.Instance.TypeIconsStates.Count; i++)
        {

            if (!PlayerPrefs.HasKey(GameManager.Instance.TypeIconsStates[i].SmokeType.ToString()))
                GameManager.Instance.TypeIconsStates[i].iconInPanel.sprite = GameManager.Instance.TypeIconsStates[i].UnactiveState;
            else
                GameManager.Instance.TypeIconsStates[i].iconInPanel.sprite = GameManager.Instance.TypeIconsStates[i].ActiveState;
        }
    }
    public void ManageStateOfButtons(string clickedSmokeType)
    {
        if (!PlayerPrefs.HasKey(clickedSmokeType))
        {
            PlayerPrefs.SetInt(clickedSmokeType, 1);
        }
            if (clickedSmokeType != null)
        {
            Debug.Log(clickedSmokeType);
            for(int i=0;i<GameManager.Instance.TypeIconsStates.Count;i++)
            {
                if (GameManager.Instance.TypeIconsStates[i].SmokeType.ToString() == clickedSmokeType)
                {
                    GameManager.Instance.TypeIconsStates[i].iconInPanel.sprite = GameManager.Instance.TypeIconsStates[i].ActiveState;
                    GameManager.Instance.TypeIconsStates[i].SelectedIcon.sprite = GameManager.Instance.TypeIconsStates[i].DefaultIcon;
                }
                else
                {
                    if (!PlayerPrefs.HasKey(GameManager.Instance.TypeIconsStates[i].SmokeType.ToString()))
                        GameManager.Instance.TypeIconsStates[i].iconInPanel.sprite = GameManager.Instance.TypeIconsStates[i].UnactiveState;
                    else
                        GameManager.Instance.TypeIconsStates[i].iconInPanel.sprite = GameManager.Instance.TypeIconsStates[i].ActiveState;
                    //GameManager.SharedInstance().HealthSlider.SetActive(false);
                }
            }
        }
        GameManager.Instance.SmokeHandler.CurrentSmokeType = (SmokeTypes)Enum.Parse(typeof(SmokeTypes), clickedSmokeType);
        GameManager.SharedInstance().EnableSmoke(clickedSmokeType);
    }
}

// pick icon states
//if(current slected == list[i]
//pick and apply unlocked sprite
//else
////pick and apply locked sprite
///
// e.g GameManager.Instance.TypeIconsStates[i].image= sprite.....