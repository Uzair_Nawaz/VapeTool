using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public const string CoinsKey = "Coins";
    public const string BgMusic = "BackgroundMusic";
    public const string InhaleSound = "InhaleSound";
    public const string ExhaleSound = "ExhaleSound";
    public const string CoughSound = "CoughSound";
    public const string ClickSound = "Click";


    public const string ShopItemsAddress = "ShopItems";
    public const string CurrentTank = "CurrentTank";
    public const string CurrentBattery = "CurrentBattery";

    public const string CurrentBgImageIndex = "CurrentBgImageIndex";
}
