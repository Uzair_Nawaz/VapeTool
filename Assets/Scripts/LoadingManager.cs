using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
public class LoadingManager : MonoBehaviour
{
    Image loadingSplash;
    public GameObject LoadingBtn, StartBtn;
    public static System.Action OnSplashInterAdded;
    public string nextSceneName; // Name of the game scene to load
    AsyncOperation asyncLoad;
    void Start()
    {
        LoadingBtn.SetActive(true);
        loadingSplash = LoadingBtn.GetComponent<Image>();
        StartBtn.SetActive(false);
        ChangeColor();
        asyncLoad = SceneManager.LoadSceneAsync(nextSceneName);
        asyncLoad.allowSceneActivation = false;
        Invoke(nameof(StartGameScene), UnityEngine.Random.Range(10, 13));
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        OnSplashInterAdded -= onSplashLoaded;
        OnSplashInterAdded += onSplashLoaded;
    }
    void ChangeColor()
    {
        Color randomColor = new Color(Random.value, Random.value, Random.value);
        loadingSplash.DOColor(randomColor, 1).OnComplete(() =>
        {
            ChangeColor();
        });
    }
    void onSplashLoaded()
    {
        OnSplashInterAdded -= onSplashLoaded;
        CancelInvoke(nameof(StartGameScene));
        Invoke(nameof(StartGameScene), 0.5f);
    }
    void StartGameScene()
    {
        OnSplashInterAdded -= onSplashLoaded;
        CancelInvoke(nameof(GameStart));
        Invoke(nameof(GameStart), 0.5f);

    }
    void GameStart()
    {
        LoadingBtn.SetActive(false);
        StartBtn.SetActive(true);
    }
    public void PlayGame()
    {
        asyncLoad.allowSceneActivation = true;
    }
}