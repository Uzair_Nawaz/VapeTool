using UnityEngine;
using UnityEngine.UI;
public enum ButtonStates
{
    Simple,
    Rings,
    Squares,
    Donuts,
    Rectangles
}

public class PurchaseRingsPopup : MonoBehaviour
{

    [SerializeField] Button PurchaseWithVideo;
    [SerializeField] Button PurchaseWithCoins;
    [SerializeField] Button Close;

    public Text Title;
    public Image refillImage;

    public bool simple, ring, sphare, donut, rectangle;
    private bool isAdPlayed = false;

    public ButtonStates checkState;
    private Inhaler inhaler;
    private void OnEnable()
    {
        GameManager.SharedInstance().DisableSmokeParent();   
    }
    void Start()
    {

        if (inhaler == null)
        {
            inhaler = GameManager.SharedInstance().HealthSlider.GetComponent<Inhaler>();
        }
        simple = false;
        ring = false;
        sphare = false;
        donut = false;
        rectangle = false;
        PurchaseWithCoins.onClick.AddListener(() =>
        {
            GameManager.SharedInstance().EnableSmokeParent();
            FindObjectOfType<SmokeHandler>().CurrentSmokeType = SmokeTypes.Simple;
            if (GameManager.SharedInstance().coinsCounter.DeductCoins(200))
            {
                PlayerPrefs.SetFloat(GameManager.RingsEnergyFillImageValueKey, 1);
                GameManager.SharedInstance().SmokeRingsButton.GetComponent<ButtonRings>().RingsEnergyFillImage.fillAmount = 1;

                gameObject.SetActive(false);
                SettingMain.OnSmokePurchase?.Invoke((SmokeTypes)checkState);
                GameManager.SharedInstance().EnableSmokeParent();
                switch (checkState)
                {
                    case ButtonStates.Simple:
                        break;
                    case ButtonStates.Rings:
                        ring = true;
                        simple = false;
                        sphare = false;
                        donut = false;
                        rectangle = false;
                        EnableSmoke();
                        break;
                    case ButtonStates.Squares:
                        sphare = true;
                        simple = false;
                        ring = false;
                        donut = false;
                        rectangle = false;
                        EnableSmoke();
                        break;
                    case ButtonStates.Donuts:
                        donut = true;
                        simple = false;
                        ring = false;
                        sphare = false;
                        rectangle = false;
                        EnableSmoke();
                        break;
                    case ButtonStates.Rectangles:
                        rectangle = true;
                        simple = false;
                        ring = false;
                        sphare = false;
                        donut = false;
                        EnableSmoke();
                        break;
                }
                //RateUS.Instance.ShowPopUp();
            }
            
                //GameManager.Instance.SettingMain.ChangeLock = false;
               
            
            //AdsManager.Instance.ShowInterstitialAd();
        });

      
        Close.onClick.AddListener(() =>
        {
            GameManager.Instance.SmokeHandler.isAlive = true;
            GameManager.SharedInstance().EnableSmokeParent();
            if (GameManager.Instance.SmokeHandler.RingsEnergyFillImage.fillAmount <= 0)
            {
                GameManager.Instance.ButtonStateManager.ManageStateOfButtons("Simple");
                GameManager.SharedInstance().HealthSlider.SetActive(false);
                GameManager.Instance.SmokeHandler.CurrentSmokeType = (SmokeTypes)ButtonStates.Simple;
                gameObject.SetActive(false);
            }
            else if(GameManager.Instance.SmokeHandler.RingsEnergyFillImage.fillAmount > 0 && GameManager.Instance.SmokeHandler.RingsEnergyFillImage.fillAmount < 1)
            {
               
                gameObject.SetActive(false);
            }
            else
            {
                gameObject.SetActive(false);
            }
        });
    }
    void AwardSmokeEnergy()
    {
        GameManager.SharedInstance().SmokeRingsButton.GetComponent<ButtonRings>().RingsEnergyFillImage.fillAmount = 1;
        gameObject.SetActive(false);
        SettingMain.OnSmokePurchase?.Invoke((SmokeTypes)checkState);
        //RateUS.Instance.ShowPopUp();
        EnableSmoke();
    }
    public void OnClickWatchVideo()
    {
            //AdsManager.ShowRewardedVideo((AdCompleted) =>
            //{
            //    if (AdCompleted)
            //    {
                    AwardSmokeEnergy();
            //    }
            //    else
            //    {
            //        AdsManager.ShowInterstitial();
            //        AwardSmokeEnergy();
            //    }

            //});
       

    }
    void EnableSmoke()
    {
        if (inhaler == null)
        {
            inhaler = GameManager.SharedInstance().HealthSlider.GetComponent<Inhaler>();
        }
        GameManager.SharedInstance().HealthSlider.SetActive(true);
        inhaler.InhaleProgressImage.fillAmount = PlayerPrefs.GetFloat(GameManager.RingsEnergyFillImageValueKey, 1);
        PlayerPrefs.SetFloat(GameManager.RingsEnergyFillImageValueKey, 1);
        PlayerPrefs.Save();
        GameManager.Instance.SmokeHandler.isAlive = true;
        GameManager.Instance.ButtonStateManager.ManageStateOfButtons(checkState.ToString());
        GameManager.SharedInstance().EnableSmokeParent();
    }
}
