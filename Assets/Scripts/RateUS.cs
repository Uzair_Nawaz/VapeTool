using System;
using UnityEngine;

public class RateUS: MonoBehaviour
{
    private static RateUS _instance;
    public static RateUS Instance { get { return _instance; } }
    public Action ShowRatingPopUp;
    private void Awake()
    {
        
        ShowRatingPopUp += ShowPopUp;
        
        if (!_instance)
        {
            _instance = this;

        }
        else
        {
            Destroy(this);
        }
    }

    public void ShowPopUp()
    {

    }


}
