using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum UIAnimStyle { ModeSelectionStart, ModeSelectionClose, VapeSelectionStart, VapeSelectionClose, GamePanelStart, GamePanelClose, ZoomPanelStart, ZoomPanelClose, ModeSelectionTutorial, VapeSelectionTutorial, GamePanelTutorial1, CamPanelStart, CamPanelClose, GamePanelTutorial2, GamePanelTutorial3, def }
public class UIAnimObjHandler : MonoBehaviour
{
    public Animator UIAnimator;
    public static UIAnimObjHandler instance;
    public GameObject ModeSelectionObj, VapeSelectionObj, GamePanelObj, ZoomPanelObj, LoadingActivate, CamPanelObj;
    const string Tutorialstr = "Tutorialstr";
    public static bool isProMode = false;
    public static System.Action OnPageChange;
    private void Awake()
    {
        instance = this;
    }
    UIAnimStyle crntActiveStyle = UIAnimStyle.def;
    private void Start()
    {
        Invoke(nameof(SetCharacterToCenter), 0.1f);

    }
    public void StartAnimAgain()
    {
        if (crntActiveStyle != UIAnimStyle.def)
        {
            PlayAnim(crntActiveStyle, () =>
            {
                CharacterController.OnCharacterPosSet?.Invoke(CharacterPos.SidePos);
            });
        }
    }
    void SetCharacterToCenter()
    {
        CharacterController.OnCharacterPosSet?.Invoke(CharacterPos.CenterPos);
    }
    private void OnDestroy()
    {
        instance = null;
    }
    bool isTutShown(UIAnimStyle mode)
    {
        return PlayerPrefs.HasKey(Tutorialstr + mode.ToString()) || PlayerPrefs.HasKey(Tutorialstr);
    }
    void UpdateTutStatus(UIAnimStyle mode)
    {
        PlayerPrefs.SetInt(Tutorialstr + mode.ToString(), 1);
        PlayerPrefs.Save();
    }
    void SkipTut()
    {
        PlayerPrefs.SetInt(Tutorialstr, 1);
        PlayerPrefs.Save();
    }
    public void ShowModeSelectionPanel()
    {
        isProMode = false;
        GameManagerVape.isGameStarted = false;
        HideCrntActivePanel(ModeSelectionObj, () =>
        {
            ModeSelectionObj.SetActive(true);
            UIAnimStyle anim = UIAnimStyle.ModeSelectionStart;
            if (!isTutShown(UIAnimStyle.ModeSelectionTutorial))
            {
                anim = UIAnimStyle.ModeSelectionTutorial;
                UpdateTutStatus(anim);
                CharacterController.OnCharacterPosSet?.Invoke(CharacterPos.SidePos);
            }
            PlayAnim(anim, () =>
            {
                GameManagerVape.isGameStarted = true;
                CharacterController.OnCharacterPosSet?.Invoke(CharacterPos.SidePos);
            });
        });
    }
    public void ShowGamePlayAnim2()
    {
        UIAnimStyle anim = UIAnimStyle.GamePanelTutorial2;

        if (!isTutShown(anim))
        {
            isProMode = false;
            GameManagerVape.isGameStarted = false;
            UpdateTutStatus(anim);
            PlayAnim(anim, () =>
            {
                GameManagerVape.isGameStarted = true;

                CharacterController.OnCharacterPosSet?.Invoke(CharacterPos.SidePos);
                ShowGamePlayAnim3();
            });
        }

    }
    public void ShowGamePlayAnim3()
    {
        UIAnimStyle anim = UIAnimStyle.GamePanelTutorial3;

        if (!isTutShown(anim))
        {
            isProMode = false;
            GameManagerVape.isGameStarted = false;
            UpdateTutStatus(anim);
            PlayAnim(anim, () =>
            {
                GameManagerVape.isGameStarted = true;

                CharacterController.OnCharacterPosSet?.Invoke(CharacterPos.SidePos);
            });
        }

    }
    public void ShowVapeSelectionPanel()
    {
        GameManagerVape.isGameStarted = false;

        isProMode = false;
        HideCrntActivePanel(VapeSelectionObj, () =>
        {
            VapeSelectionObj.SetActive(true);
            UIAnimStyle anim = UIAnimStyle.VapeSelectionStart;
            if (!isTutShown(UIAnimStyle.VapeSelectionTutorial))
            {
                anim = UIAnimStyle.VapeSelectionTutorial;
                UpdateTutStatus(anim);
            }
            PlayAnim(anim, () =>
            {
                GameManagerVape.isGameStarted = true;

                CharacterController.OnCharacterPosSet?.Invoke(CharacterPos.SidePos);
            });
        });
    }
    public void ShowGamePanel()
    {
        GameManagerVape.isGameStarted = false;

        isProMode = false;
        HideCrntActivePanel(GamePanelObj, () =>
        {
            GamePanelObj.SetActive(true);
            UIAnimStyle anim = UIAnimStyle.GamePanelStart;
            GameManagerVape.instance.StopProMode();
            if (!isTutShown(UIAnimStyle.GamePanelTutorial1))
            {
                anim = UIAnimStyle.GamePanelTutorial1;
                UpdateTutStatus(anim);
            }
            PlayAnim(anim, () =>
            {
                GameManagerVape.isGameStarted = true;

                CharacterController.OnCharacterPosSet?.Invoke(CharacterPos.SidePos);
            });
        });
    }
    public void showCamPanel()
    {
        GameManagerVape.isGameStarted = false;

        isProMode = true;
        HideCrntActivePanel(CamPanelObj, () =>
        {
            CamPanelObj.SetActive(true);
            CharacterController.OnCharacterPosSet?.Invoke(CharacterPos.SidePos);
            if (GameManagerVape.instance.StartProMode())
            {
                PlayAnim(UIAnimStyle.CamPanelStart, () =>
                {
                    GameManagerVape.isGameStarted = true;

                });
            }
            else
            {
                ShowGamePanel();
            }

        });
    }
    public void showZoomPanel()
    {
        GameManagerVape.isGameStarted = false;

        isProMode = false;
        HideCrntActivePanel(ZoomPanelObj, () =>
        {
            ZoomPanelObj.SetActive(true);
            CharacterController.OnCharacterPosSet?.Invoke(CharacterPos.OutOfCam);
            PlayAnim(UIAnimStyle.ZoomPanelStart, () =>
            {
                GameManagerVape.isGameStarted = true;

            });
        });
    }
    void HideCrntActivePanel(GameObject GOToIGnore, System.Action callback)
    {
        OnPageChange?.Invoke();
        int val = 0;
        if (ModeSelectionObj.activeInHierarchy && ModeSelectionObj != GOToIGnore)
        {
            PlayAnim(UIAnimStyle.ModeSelectionClose, () =>
            {
                val = CheckForCallBack(val, callback);
                ModeSelectionObj.SetActive(false);
            });
        }
        else
        {
            val = CheckForCallBack(val, callback);
        }
        if (VapeSelectionObj.activeInHierarchy && VapeSelectionObj != GOToIGnore)
        {
            PlayAnim(UIAnimStyle.VapeSelectionClose, () =>
            {
                val = CheckForCallBack(val, callback);
                VapeSelectionObj.SetActive(false);
            });
        }
        else
        {
            val = CheckForCallBack(val, callback);

        }
        if (GamePanelObj.activeInHierarchy && GamePanelObj != GOToIGnore)
        {
            PlayAnim(UIAnimStyle.GamePanelClose, () =>
            {
                val = CheckForCallBack(val, callback);
                GamePanelObj.SetActive(false);
            });
        }
        else
        {
            val = CheckForCallBack(val, callback);

        }
        if (ZoomPanelObj.activeInHierarchy && ZoomPanelObj != GOToIGnore)
        {
            PlayAnim(UIAnimStyle.ZoomPanelClose, () =>
            {
                val = CheckForCallBack(val, callback);
                ZoomPanelObj.SetActive(false);
            });
        }
        else
        {
            val = CheckForCallBack(val, callback);

        }
        if (CamPanelObj.activeInHierarchy && CamPanelObj != GOToIGnore)
        {
            PlayAnim(UIAnimStyle.CamPanelClose, () =>
            {
                val = CheckForCallBack(val, callback);
                CamPanelObj.SetActive(false);
            });
        }
        else
        {
            val = CheckForCallBack(val, callback);

        }
        LoadingActivate.SetActive(false);

    }
    int CheckForCallBack(int val, System.Action Callback)
    {
        val += 1;
        if (val >= 5)
        {
            Callback?.Invoke();
        }
        return val;
    }
    public void PlayAnim(UIAnimStyle style, System.Action callback)
    {
        crntActiveStyle = style;
        UIAnimator.Play(style.ToString());
        StartCoroutine(WaitForAnimationComplete(style.ToString(), callback));
    }
    private IEnumerator WaitForAnimationComplete(string animationName, System.Action callback)
    {
        // Get the length of the animation
        AnimationClip clip = UIAnimator.runtimeAnimatorController.animationClips.FirstOrDefault(a => a.name == animationName);
        if (clip != null)
        {
            float animationLength = clip.length;
            yield return Yielders.Get(animationLength);

            // Animation has completed, invoke your desired function here
            callback?.Invoke();
            if (crntActiveStyle.ToString().Equals(animationName))
            {
                crntActiveStyle = UIAnimStyle.def;
            }
        }
        else
        {
            yield return Yielders.Get(3);
            callback?.Invoke();
            if (crntActiveStyle.ToString().Equals(animationName))
            {
                crntActiveStyle = UIAnimStyle.def;
            }
        }
    }
}
