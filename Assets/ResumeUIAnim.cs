using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResumeUIAnim : MonoBehaviour
{
    private void OnEnable()
    {
        if(UIAnimObjHandler.instance)
        UIAnimObjHandler.instance.StartAnimAgain();
    }
}
