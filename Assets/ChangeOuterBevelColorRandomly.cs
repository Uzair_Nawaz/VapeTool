using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class ChangeOuterBevelColorRandomly : MonoBehaviour
{
    Bevel _bevel;
    private void Start()
    {
        _bevel = this.GetComponent<Bevel>();
        ChangeTextColorRandomly();
    }
    void ChangeTextColorRandomly()
    {
        if (_bevel)
        {
            Color tempColor = new Color(Random.Range(0.01f, 0.99f), Random.Range(0.01f, 0.99f), Random.Range(0.01f, 0.99f), 1);
            DOVirtual.Color(_bevel.shadowColor, tempColor,0.5f, (value) =>
            {
                _bevel.shadowColor = value;
            }).OnComplete(() =>
            {
                ChangeTextColorRandomly();
            });
        }
    }
}
