using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ItemPurchasePopup : MonoBehaviour
{
    private ShopItem SelectedItem;
    [SerializeField]
    Image ProductImage;
    [SerializeField]
    Button BuyViaCoins;
    [SerializeField]
    Button Close;

    void Start()
    {
        Close.onClick.AddListener(()=>
        {
            transform.DOScale(0, 0.3f);
            Invoke(nameof(ClosePopUp), 0.3f);
        });

        BuyViaCoins.onClick.AddListener(()=>
        {
            AudioManager.SharedInstance().Play_Sound(Constants.ClickSound);
            bool coins_duducted = GameManager.SharedInstance().coinsCounter.DeductCoins(200);
            if (coins_duducted)
            {
                GameManager.SharedInstance().StoreCurrentItemInPrefs(SelectedItem.id);

                GameManager.SharedInstance().current_vape.SetVapeAccessories();
            }
            else
            {
                GameManager.SharedInstance().WatchAVideo.SetActive(true);
            }
            GameManager.SharedInstance().ItemPurchasePopup.SetActive(false);
        });
    }

    private void ClosePopUp()
    {
        gameObject.SetActive(false);
    }

    void DisplayProductSpecs()
    {
        ProductImage.sprite = SelectedItem.productImage.sprite;
    }

    private void OnEnable()
    {
        transform.DOScale(1, 0.3f);
        SelectedItem = GameManager.SharedInstance().Shop.GetComponent<Shop>().Selected_item;
        DisplayProductSpecs();
    }
}
