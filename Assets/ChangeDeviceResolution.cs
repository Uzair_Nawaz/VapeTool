using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeDeviceResolution : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if(CheckForScreenSize.DeviceType()== GetDeviceType.Tablet)
        {
            this.GetComponent<CanvasScaler>().referenceResolution = new Vector2(1080, 1280);
        }
        else
        {
            this.GetComponent<CanvasScaler>().referenceResolution = new Vector2(826, 1280);


        }
    }
}
