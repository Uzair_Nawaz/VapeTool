using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent((typeof(Button)))]
public class BtnIndexHandler : MonoBehaviour
{
    public int MySmokingIndex;
    private void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(() => {
            VapeUnlockPopupHandler vapeUnlockPopupHandler = GameManager.Instance.VapeUnlockPopupHandler;
            vapeUnlockPopupHandler.gameObject.SetActive(true);
            vapeUnlockPopupHandler.Init(MySmokingIndex);
            //PopupInitialize;
        });
    }
}
