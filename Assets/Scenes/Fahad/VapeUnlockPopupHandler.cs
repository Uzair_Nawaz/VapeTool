using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VapeUnlockPopupHandler : MonoBehaviour
{
    SmokeTypes CurrentSelectedSMokeType;
    public void Init(int Val)
    {
        CurrentSelectedSMokeType = (SmokeTypes)Val;
        Debug.Log(Val);
    }
    public void UnlockWithCoin()
    {
        // todo if (Success)
        //{
        //    EnableSmoke()
        //}
    }
    public void UnlockWithAd()
    {
        // todo if (Success)
        //{
        //    EnableSmoke()
        //}
    }
    void EnableSmoke()
    {
        GameManager.SharedInstance().HealthSlider.SetActive(true);
        GameManager.Instance.SmokeHandler.isAlive = true;
        GameManager.Instance.ButtonStateManager.ManageStateOfButtons(CurrentSelectedSMokeType.ToString());
    }
}
