using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Image))]
public class buttonSelectionHandler : MonoBehaviour
{
    public int index;
    private void Start()
    {
        SmokeController.OnSmokeShapeUpdated -= UpdateSmokeSelection;
        SmokeController.OnSmokeShapeUpdated += UpdateSmokeSelection;
        this.GetComponent<Button>().onClick.AddListener(() =>
        {
            SmokeShapeHandler.UserPressSmokeShapeBtn(index);
        });
    }
    void UpdateSmokeSelection(int id)
    {
        if (index == id)
        {
            this.GetComponent<Image>().material = GameManagerVape.instance.SelectedMaterial;
        }
        else
        {
            this.GetComponent<Image>().material = GameManagerVape.instance.UnSelectedMaterial;
        }
    }
}
